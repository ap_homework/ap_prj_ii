Enrollment Number : PhD1070
Timestamp : Tue Feb 24 06:37:34 IST 2015

PERSONAL DETAILS:
Email : justinemoriarty@iiitd.ac.in
Name : Justine Moriarty
Address of Correspondence : 151 Cheltenham Gardens, Hedge End, Southampton, Hampshire SO30 4UE, India
Mobile : 8652494041
PhD Stream : Computer Science
PhD Preference 1 : Mobile Computing
PhD Preference 2 : 
PhD Preference 3 : 
Gender : Female
Category : OBC
Physically Disabled : No
Date of Birth : 16-09-1991
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Wade Moriarty
Nationality : Indian
Permanent Address : 151 Cheltenham Gardens, Hedge End, Southampton, Hampshire SO30 4UE, UK
Pincode : 945866
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : CBSE
Xth Marks : 76.0
Year of Passing Xth : 2007
XIIth Board : CBSE
XIIth Marks : 84.0
Year of Passing XIIth : 2009
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Computer Science
College : IIIT-Hyderabad
University : IIIT-Hyderabad
City : Hyderabad
State : Andhra Pradesh
Year of Graduation : 2013
Marks : 89.0%
----------------------------------------------------------
POST GRADUATION
College : IIIT-Hyderabad
City : Hyderabad
State : Andhra Pradesh
Department/Discipline : Computer Science
Thesis title : Random Title
Year of Post-Graduation : 2015
Marks : 77.0%
--------------------------------------------------------------
Achievements :
