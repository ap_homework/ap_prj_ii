Enrollment Number : PhD1014
Timestamp : Sun Mar 15 00:54:08 IST 2015

PERSONAL DETAILS:
Email : jenisestirling@iiitd.ac.in
Name : Jenise Stirling
Address of Correspondence : 7 Oak Tree Ave, Edwinstowe, Mansfield, Nottinghamshire NG21 9NF, India
Mobile : 9597767749
PhD Stream : Electronics and Communication
PhD Preference 1 : VLSI
PhD Preference 2 : Embedded Systems
PhD Preference 3 : Embedded System
Gender : Female
Category : General
Physically Disabled : No
Date of Birth : 24-07-1990
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Lyman Stirling
Nationality : Pakistani
Permanent Address : 7 Oak Tree Ave, Edwinstowe, Mansfield, Nottinghamshire NG21 9NF, UK
Pincode : 160748
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : ICSE
Xth Marks : 90.0
Year of Passing Xth : 2006
XIIth Board : ICSE
XIIth Marks : 76.0
Year of Passing XIIth : 2008
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Electronics and Communication
College : NIT-Surathkal
University : NIT
City : Surathkal
State : Karnataka
Year of Graduation : 2012
Marks : 81.0%
----------------------------------------------------------
ECE PhD
Preference 1 : VLSI
Preference 2 : Embedded Systems
Preference 3 : Embedded System
Preference 4 : 
--------------------------------------------------------
POST GRADUATION
College : NIT-Surathkal
City : Surathkal
State : Karnataka
Department/Discipline : Electronics and Communication
Thesis title : Random Title
Year of Post-Graduation : 2014
Marks : 76.0%
--------------------------------------------------------------
Achievements :
