Enrollment Number : PhD1066
Timestamp : Tue Apr 28 09:47:28 IST 2015

PERSONAL DETAILS:
Email : connieikerd@iiitd.ac.in
Name : Connie Ikerd
Address of Correspondence : Church Ln, Birmingham, Warwickshire B46 2QW, India
Mobile : 9975933393
PhD Stream : Computational Biology
PhD Preference 1 : BioSimulations
PhD Preference 2 : Data Processing
PhD Preference 3 : Molecular Biology
Gender : Male
Category : General
Physically Disabled : No
Date of Birth : 04-07-1991
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Cody Ikerd
Nationality : Indian
Permanent Address : Church Ln, Birmingham, Warwickshire B46 2QW, UK
Pincode : 630260
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : CBSE
Xth Marks : 70.0
Year of Passing Xth : 2007
XIIth Board : CBSE
XIIth Marks : 89.0
Year of Passing XIIth : 2009
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Computer Science
College : NIT-Warangal
University : NIT
City : Warangal
State : Andhra Pradesh
Year of Graduation : 2013
Marks : 97.0%
----------------------------------------------------------
POST GRADUATION
College : NIT-Warangal
City : Warangal
State : Andhra Pradesh
Department/Discipline : Computer Science
Thesis title : Random Title
Year of Post-Graduation : 2015
Marks : 79.0%
--------------------------------------------------------------
Achievements :
