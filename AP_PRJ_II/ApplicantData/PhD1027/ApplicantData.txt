Enrollment Number : PhD1027
Timestamp : Sat Jan 24 15:50:04 IST 2015

PERSONAL DETAILS:
Email : delaineformica@iiitd.ac.in
Name : Delaine Formica
Address of Correspondence : 11 Portpool Ln, London EC1N, India
Mobile : 7879417879
PhD Stream : Computational Biology
PhD Preference 1 : Image Processing
PhD Preference 2 : Mathematical Biology
PhD Preference 3 : 
Gender : Male
Category : General
Physically Disabled : No
Date of Birth : 04-07-1986
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Jaime Formica
Nationality : Indian
Permanent Address : 11 Portpool Ln, London EC1N, UK
Pincode : 649784
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : ICSE
Xth Marks : 79.0
Year of Passing Xth : 2002
XIIth Board : ICSE
XIIth Marks : 77.0
Year of Passing XIIth : 2004
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Computer Science
College : PSG College of Technology
University : Anna University
City : Coimbatore
State : Tamil Nadu
Year of Graduation : 2008
Marks : 81.0%
----------------------------------------------------------
POST GRADUATION
College : PSG College of Technology
City : Coimbatore
State : Tamil Nadu
Department/Discipline : Computer Science
Thesis title : Random Title
Year of Post-Graduation : 2010
Marks : 77.0%
--------------------------------------------------------------
Achievements :
