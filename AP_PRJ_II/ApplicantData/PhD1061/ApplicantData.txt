Enrollment Number : PhD1061
Timestamp : Thu Mar 12 22:17:33 IST 2015

PERSONAL DETAILS:
Email : verlabains@iiitd.ac.in
Name : Verla Bains
Address of Correspondence : Elliots End, Scraptoft, Leicester, Leicestershire LE7, India
Mobile : 9951354542
PhD Stream : Computer Science
PhD Preference 1 : Information Security
PhD Preference 2 : 
PhD Preference 3 : 
Gender : Male
Category : OBC
Physically Disabled : No
Date of Birth : 20-03-1989
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Mitchell Bains
Nationality : Indian
Permanent Address : Elliots End, Scraptoft, Leicester, Leicestershire LE7, UK
Pincode : 329738
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : State Board
Xth Marks : 72.0
Year of Passing Xth : 2005
XIIth Board : State Board
XIIth Marks : 70.0
Year of Passing XIIth : 2007
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Information Technology
College : PSG College of Technology
University : Anna University
City : Coimbatore
State : Tamil Nadu
Year of Graduation : 2011
Marks : 82.0%
----------------------------------------------------------
POST GRADUATION
College : PSG College of Technology
City : Coimbatore
State : Tamil Nadu
Department/Discipline : Information Technology
Thesis title : Random Title
Year of Post-Graduation : 2013
Marks : 76.0%
--------------------------------------------------------------
Area : Computer Science And IT
Year : 2011
Marks : 73.0/100
Score : 766.0
Rank : 5151
--------------------------------------------------------------
Achievements :
