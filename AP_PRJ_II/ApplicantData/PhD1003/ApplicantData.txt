Enrollment Number : PhD1003
Timestamp : Fri Apr 24 08:28:40 IST 2015

PERSONAL DETAILS:
Email : cynthiawhalley@iiitd.ac.in
Name : Cynthia Whalley
Address of Correspondence : 1 Sycamore Grove, Knowbury, Ludlow, Shropshire SY8 3JW, India
Mobile : 9823807309
PhD Stream : Computational Biology
PhD Preference 1 : Image Processing
PhD Preference 2 : Data Processing
PhD Preference 3 : Molecular Biology
Gender : Male
Category : General
Physically Disabled : No
Date of Birth : 09-05-1986
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Son Whalley
Nationality : Indian
Permanent Address : 1 Sycamore Grove, Knowbury, Ludlow, Shropshire SY8 3JW, UK
Pincode : 167801
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : CBSE
Xth Marks : 78.0
Year of Passing Xth : 2002
XIIth Board : CBSE
XIIth Marks : 70.0
Year of Passing XIIth : 2004
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Computer Science
College : Delhi Technological University
University : Delhi Technological University
City : New Delhi
State : Delhi
Year of Graduation : 2008
CGPA: 8.4/10.0
----------------------------------------------------------
POST GRADUATION
College : Delhi Technological University
City : New Delhi
State : Delhi
Department/Discipline : Computer Science
Thesis title : Random Title
Year of Post-Graduation : 2010
Marks : 70.0%
--------------------------------------------------------------
Area : Computer Science And IT
Year : 2008
Marks : 74.0/100
Score : 777.0
Rank : 6464
--------------------------------------------------------------
Achievements :
