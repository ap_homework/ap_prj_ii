Enrollment Number : PhD1033
Timestamp : Fri Sep 11 06:05:46 IST 2015

PERSONAL DETAILS:
Email : lesafomby@iiitd.ac.in
Name : Lesa Fomby
Address of Correspondence : 22 Southwold Pl, Cramlington, Northumberland NE23 8HE, India
Mobile : 8939033524
PhD Stream : Computational Biology
PhD Preference 1 : Image Processing
PhD Preference 2 : 
PhD Preference 3 : 
Gender : Male
Category : General
Physically Disabled : No
Date of Birth : 17-02-1990
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Eddy Fomby
Nationality : Indian
Permanent Address : 22 Southwold Pl, Cramlington, Northumberland NE23 8HE, UK
Pincode : 439246
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : State Board
Xth Marks : 80.0
Year of Passing Xth : 2006
XIIth Board : State Board
XIIth Marks : 79.0
Year of Passing XIIth : 2008
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Computer Science
College : Harcourt Butler Technological Institute
University : UPTU
City : Kanpur
State : Uttar Pradesh
Year of Graduation : 2012
Marks : 77.0%
----------------------------------------------------------
POST GRADUATION
College : Harcourt Butler Technological Institute
City : Kanpur
State : Uttar Pradesh
Department/Discipline : Computer Science
Thesis title : Random Title
Year of Post-Graduation : 2014
CGPA : 8.4/10.0
--------------------------------------------------------------
Achievements :
