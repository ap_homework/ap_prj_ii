Enrollment Number : PhD1056
Timestamp : Tue Apr 28 10:52:20 IST 2015

PERSONAL DETAILS:
Email : ernestinaflohr@iiitd.ac.in
Name : Ernestina Flohr
Address of Correspondence : Great Wyrley, Staffordshire, India
Mobile : 7415173953
PhD Stream : Electronics and Communication
PhD Preference 1 : VLSI
PhD Preference 2 : Distributed Systems
PhD Preference 3 : Embedded System
Gender : Female
Category : SC
Physically Disabled : No
Date of Birth : 23-08-1987
Children/War Widows of Defence Personnel wounded/killed in war : No
Father's Name : Clark Flohr
Nationality : Saudi
Permanent Address : Great Wyrley, Staffordshire, UK
Pincode : 615336
--------------------------------------------------------------
SCHOOLING DETAILS:
Xth Board : State Board
Xth Marks : 94.0
Year of Passing Xth : 2003
XIIth Board : State Board
XIIth Marks : 71.0
Year of Passing XIIth : 2005
--------------------------------------------------------------
GRADUATION DETAILS:
Degree : B.Tech
Department/Discipline : Electronics and Communication
College : VJTI
University : VJTI
City : Mumbai
State : Maharashtra
Year of Graduation : 2009
Marks : 84.0%
----------------------------------------------------------
ECE PhD
Preference 1 : VLSI
Preference 2 : Distributed Systems
Preference 3 : Embedded System
Preference 4 : 
--------------------------------------------------------
POST GRADUATION
College : VJTI
City : Mumbai
State : Maharashtra
Department/Discipline : Electronics and Communication
Thesis title : Random Title
Year of Post-Graduation : 2011
Marks : 86.0%
--------------------------------------------------------------
Achievements :
