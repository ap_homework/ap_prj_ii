function Stream(id)
					{
						var csepref = ["Artificial Intelligence and Robotics - CSE", "Compilers - CSE",
	   					"Computer Architecture and Systems Design - CSE", "Computer Graphics - CSE", "Computer Vision - CSE","Image Analysis and Biometrics - CSE", "Information Management and Data Engineering - CSE","Machine Learning - CSE", "Massively Parallel Systems - CSE","Mobile Computing and Networking Applications - CSE", "Program Analysis - CSE","Security and Privacy - CSE", "Signal and Image Processing - CSE", "Software Engineering - CSE","Theoretical Computer Science - CSE", "Wireless Networks - CSE"];
						var ecepref = ["Computer Architecture and Systems Design - ECE", "Controls and Robotics - ECE",
	   					"Digital and Analog VLSI Systems Design - ECE", "Electromagnetics - ECE","Embedded and VLSI systems design - ECE", "Embedded Systems - ECE", "Fiber-Wireless Architectures - ECE","Machine Learning - ECE", "OFDM based Optical Access Networks - ECE","Optical Wireless Communication Systems - ECE", "RF and mixed signal electronics - ECE","Signal and Image Processing - ECE", "Wireless Communication - ECE", "Wireless Networks - ECE"];
						var cbpref = ["Biophysics - CB","Structural Biology - CB", "Systems Biology - CB"];
						var options;
						if(id == 'cse'){
							options = csepref;
						}
						else if( id == 'ece'){
							options = ecepref;
						}
						else {
							options = cbpref;
						}
						var pref1 = document.getElementById("id_phd_area_pref1"); 
						var pref2 = document.getElementById("id_phd_area_pref2"); 
						var pref3 = document.getElementById("id_phd_area_pref3");
						// for(var i=pref1.options.length-1;i>=1;i--)
						// 	pref1.remove(i);
						// for(var i=pref2.options.length-1;i>=1;i--)
						// 	pref2.remove(i);
						// for(var i=pref3.options.length-1;i>=1;i--)
						// 	pref3.remove(i);
						for(var i=0;i<pref1.options.length;i++)
							document.write(pref1.options[i]);
						for(var i=0;i<pref2.options.length;i++)
							document.write(pref2.options[i]);
						for(var i=0;i<pref3.options.length;i++)
							document.write(pref3.options[i]);
						for(var i = 0; i < options.length; i++) {
						    var opt = options[i];
						    var el = document.createElement("option");
						    el.textContent = opt;
						    el.value = opt;
						    pref1.appendChild(el);
						    pref2.appendChild(el);
						    pref3.appendChild(el);
						}
					}
					Stream('cse');