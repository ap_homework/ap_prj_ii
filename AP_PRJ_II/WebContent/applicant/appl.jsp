<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- ISO-8859-1 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>IIIT-Delhi PhD Admissions</title>

<script type="text/javascript" src="l.js" data-processed="1"></script>
<script type="text/javascript" src="l(1).js" data-processed="1"></script>
<script type="text/javascript" src="l(2).js" data-processed="1"></script>
<script type="text/javascript" src="gteamqc.js"></script>
<script type="text/javascript" src="gteamrs.js"></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" media="all" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="bootstrap-datetimepicker.min.css" type="text/css" media="all" rel="stylesheet">
<script type="text/javascript" src="bootstrap.min.js"></script>
<script type="text/javascript" src="moment.min.js"></script>
<script type="text/javascript" src="bootstrap-datetimepicker.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script>
$(document).ready(function() { $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' }); });
</script>

<script type="text/javascript">
    
</script>

<script type="text/javascript">
$(function () {
    $('fieldset.is_ece').hide();
    $('input[name="is_ece"]').click(function () {
        if (this.checked) {
            $('fieldset.is_ece').show();
        } else {
            $('fieldset.is_ece').hide();
        }
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('fieldset.is_pg').hide();
    $('input[name="is_pg"]').click(function () {
        if (this.checked) {
            $('fieldset.is_pg').show();
        } else {
            $('fieldset.is_pg').hide();
        }
    });
});
</script>
<script type="text/javascript">
$(function () {
    $('fieldset.is_other').hide();
    $('input[name="is_other"]').click(function () {
        if (this.checked) {
            $('fieldset.is_other').show();
        } else {
            $('fieldset.is_other').hide();
        }
    });
});
</script>

<script type="text/javascript">
$(function () {
    $('fieldset.is_gate').hide();
    $('input[name="is_gate"]').click(function () {
        if (this.checked) {
            $('fieldset.is_gate').show();
        } else {
            $('fieldset.is_gate').hide();
        }
    });
});
</script>
<script>
	var savePer = false;
	var saveEdu = false;
    function enableSubmit1() {
        savePer = true;
    	if(saveEdu && savePer)
    	document.getElementById("submit_button").disabled = false;
    	var d1=new Date();
    	var d2=new Date(Document.getElementsByName("dob"));
    	if(d2>d1)
    		alert("Incorrect date entered.");
    }
    function enableSubmit2(){
    	saveEdu = true;
    	if(savePer && saveEdu)
    	document.getElementById("submit_button").disabled = false;
    	var xyr=parseInt(Document.getElementsByName("x_year"));
    	var xiiyr=parseInt(Document.getElementsByName("xii-year"));
    	var uyear=parseInt(Document.getElementsByName("u_year"));
    	var pyear=parseInt(Document.getElementsByName("p_year"));
    	var gradyear=parseInt(Document.getElementsByName("grad_year"));
    	if(xyr>xiiyr || xiiyr>uyear || uyear>gradyear)
    		alert("Incorrect data entered.");
    }
</script>
<script type="text/javascript">
    				function Stream(id)
					{
						var csepref = ["Artificial Intelligence and Robotics - CSE", "Compilers - CSE",
	   					"Computer Architecture and Systems Design - CSE", "Computer Graphics - CSE", "Computer Vision - CSE","Image Analysis and Biometrics - CSE", "Information Management and Data Engineering - CSE","Machine Learning - CSE", "Massively Parallel Systems - CSE","Mobile Computing and Networking Applications - CSE", "Program Analysis - CSE","Security and Privacy - CSE", "Signal and Image Processing - CSE", "Software Engineering - CSE","Theoretical Computer Science - CSE", "Wireless Networks - CSE"];
						var ecepref = ["Computer Architecture and Systems Design - ECE", "Controls and Robotics - ECE",
	   					"Digital and Analog VLSI Systems Design - ECE", "Electromagnetics - ECE","Embedded and VLSI systems design - ECE", "Embedded Systems - ECE", "Fiber-Wireless Architectures - ECE","Machine Learning - ECE", "OFDM based Optical Access Networks - ECE","Optical Wireless Communication Systems - ECE", "RF and mixed signal electronics - ECE","Signal and Image Processing - ECE", "Wireless Communication - ECE", "Wireless Networks - ECE"];
						var cbpref = ["Biophysics - CB","Structural Biology - CB", "Systems Biology - CB"];
						var options;
						if(id == 'cse'){
							options = csepref;
						}
						else if( id == 'ece'){
							options = ecepref;
						}
						else if( id == 'cb'){
							options = cbpref;
						}
						var pref1 = document.getElementById("id_phd_area_pref1"); 
						var pref2 = document.getElementById("id_phd_area_pref2"); 
						var pref3 = document.getElementById("id_phd_area_pref3");
						for(var i=pref1.options.length-1;i>=1;i--)
							pref1.remove(i);
						for(var i=pref2.options.length-1;i>=1;i--)
							pref2.remove(i);
						for(var i=pref3.options.length-1;i>=1;i--)
						 	pref3.remove(i);
						for(var i=0;i<pref1.options.length;i++)
							console.log(pref1.options[i]);
						for(var i=0;i<pref2.options.length;i++)
							console.log(pref2.options[i]);
						for(var i=0;i<pref3.options.length;i++)
							console.log(pref3.options[i]);
						for(var i = 0; i < options.length; i++) {
						    var opt = options[i];
						    var el = document.createElement("option");
						    el.textContent = opt;
						    el.value = opt;
						    pref1.appendChild(el);
						}
						for(var i = 0; i < options.length; i++) {
						    var opt = options[i];
						    var el = document.createElement("option");
						    el.textContent = opt;
						    el.value = opt;
						    pref2.appendChild(el);
						}
						for(var i = 0; i < options.length; i++) {
						    var opt = options[i];
						    var el = document.createElement("option");
						    el.textContent = opt;
						    el.value = opt;
						    pref3.appendChild(el);
						}
					}
					</script>



<!--  phD pref mein kuch prob hai (pinfo) .... n baki maine check nhi kiya hai....n neeche htodi formatting rehti hai
apart from this reading parameters is left....vo kaam tera..... ismein har field ka name etc dekh liyo ...
*har field ka name n value(jisse servlet mein compare karega dono dekh k servlet likhoyo
*servlet -PHDForm.java(Applicant package) do post mein read kariyo 
*abhi jo bhi kuch usmien uda de
*servlet mein ExtraClasses ki class bana n saaare parameters uske subclasses mei set krde
*end mein write ExtraClasses ko call krna hai
*time of apply bhi maintain krna hai
*Session bhi daalna hai.... (usko actualy kyun daalna hai dunno ...bt dekh liyo ki 
 2thread mein data same write na ho
 *minor changes can be done later ....in css or form
 *file upload bhi krna hhoga shayd....though ismein hona chhaiye
 *baki dekh liyo...abhi itna hi dimmag mein aaya.
 *erase mat krna....kaam aayega...:P
 -->
















</head>
<body>
	<div id="wrap" class="container content">
        <div id="wrap" class="container">
            <div class="alert alert-info">
                <form action="/AP_PRJ_II/Logout" method="POST">
                <input type="submit" value="Logout" style="float: right;">
                Submit button appears on successful completion of all forms fields.
                </form>
            </div>
            <ul class="nav nav-tabs" id="form-tabs">
                <li class="active">
                    <a href="#pinfo" data-toggle="tab">Personal Information</a>
                </li>
                <li>
                    <a href="#einfo" data-toggle="tab">Education Information</a>
                </li>
                <li>
                    <a href="#submit" data-toggle="tab">Submit</a>
                </li>
            </ul>

            <form  action="/AP_PRJ_II/PhDForm" method="POST" id="form" class="form-horizontal" role="form" enctype="multipart/form-data">
                <div id="myTabContent" class="tab-content well">
                    <div class="tab-pane fade  active in" id="pinfo">
                    <!-- form action="applicant/appl.jsp" method="POST" id="form1" class="form-horizontal" role="form" enctype="multipart/form-data" novalidate=""-->
                    <div id="div_id_email" class="form-group">
                            <label for="id_email" class="control-label col-lg-2 requiredField">Email<span class="asteriskField">*</span>
                            </label>
                        <div class="controls col-lg-10">
                            <input class="emailinput form-control" data-parsley-maxlength="75" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-type="email" id="id_email" maxlength="75" name="email" type="email">
                        </div>
                    </div>
                    <div id="div_id_name" class="form-group">
                    	<label for="id_name" class="control-label col-lg-2 requiredField">Name<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-10">
    						<input class="textinput textInput form-control" data-parsley-maxlength="128" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_name" maxlength="128" name="name" type="text"> 
    					</div>
    				</div>
       				<div id="div_id_address" class="form-group">
    					<label for="id_address" class="control-label col-lg-2 requiredField">Address of Correspondence<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-10">
    						<textarea class="textarea form-control" cols="40" data-parsley-maxlength="512" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_address" maxlength="512" name="address" rows="4"></textarea>
    					</div>
    				</div>
    				<div id="div_id_mobile" class="form-group">
    					<label for="id_mobile" class="control-label col-lg-2 requiredField">Mobile<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-10">
    						<input class="numberinput form-control" data-parsley-max="9223372036854775807" data-parsley-min="-9223372036854775808" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-type="digits" data-parsley-type-digits-message="Enter a whole number." id="id_mobile" max="9223372036854775807" min="-9223372036854775808" name="mobile" type="number""> 
    					</div>
    				</div>
                 
                  	<div id="div_id_phd_stream" class="form-group form-inline">
                  		<label for="id_phd_stream_0" class="control-label col-lg-2 requiredField">PhD Stream<span class="asteriskField">*</span></label>
                  		<div class="controls col-lg-4">
	                  		<label class="radio">
	                 			<input type="radio" name="phd_stream" id="id_phd_stream_1" onclick="Stream('cse');" value="CSE" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="phd_stream" data-parsley-id="4322">Computer Science
	                 		</label>
	           				<label class="radio">
	           				 	<input type="radio" name="phd_stream" id="id_phd_stream_2" onclick="Stream('ece');" value="ECE" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="phd_stream" data-parsley-id="4322">Electronics and Communication
	            			</label>
	            			<label class="radio">
	            			 	<input type="radio" name="phd_stream" id="id_phd_stream_3" onclick="Stream('cb');" value="CB" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="phd_stream" data-parsley-id="4322">Computational Biology
	            			</label>
            			</div>
            		</div>
					             	           		
            		<div id="div_id_phd_area_pref1" class="form-group">
            			<label for="id_phd_area_pref1" class="control-label col-lg-2 requiredField">PhD Area Preference 1<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-4">
    						<select id="id_phd_area_pref1" class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." name="phd_area_pref1" data-parsley-id="3632">
    						<option value="" selected="selected">---------</option>					
    						</select>
    						
    						<ul class="parsley-errors-list" id="parsley-id-3632">
    						</ul>
    					</div>
    				</div>
    				<div id="div_id_phd_area_pref2" class="form-group">
            			<label for="id_phd_area_pref2" class="control-label col-lg-2 requiredField">PhD Area Preference 2<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-4">
    						<select id="id_phd_area_pref2" class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." name="phd_area_pref2" data-parsley-id="0443">
    						<option value="" selected="selected">---------</option>					
    						</select>
    						
    						<ul class="parsley-errors-list" id="parsley-id-0443">
    						</ul>
    					</div>
    				</div>
    				<div id="div_id_phd_area_pref3" class="form-group">
            			<label for="id_phd_area_pref3" class="control-label col-lg-2 requiredField">PhD Area Preference 3<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-4">
    						<select id="id_phd_area_pref3" class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." name="phd_area_pref3" data-parsley-id="9035">
    						<option value="" selected="selected">---------</option>					
    						</select>
    						
    						<ul class="parsley-errors-list" id="parsley-id-9035">
    						</ul>
    					</div>
    				</div>
    				
    				<div id="div_id_gender" class="form-group form-inline">
    					<label for="id_gender_0" class="control-label col-lg-2 requiredField">Gender<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-4">
    						<label class="radio"><input type="radio" name="gender" id="id_gender_1" value="male" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="gender" data-parsley-id="6563">Male</label>
            				<ul class="parsley-errors-list" id="parsley-id-multiple-gender"></ul>
            				<label class="radio"><input type="radio" name="gender" id="id_gender_2" value="female" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="gender" data-parsley-id="6563">Female</label>
            			</div>
            		</div>
            		<div id="div_id_category" class="form-group form-inline">
            			<label for="id_category_0" class="control-label col-lg-2 requiredField">Category<span class="asteriskField">*</span></label>
            			<div class="controls col-lg-4">
            				<label class="radio"><input type="radio" name="category" id="id_category_1" value="general" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="category" data-parsley-id="6472">General</label>
            				<ul class="parsley-errors-list" id="parsley-id-multiple-category"></ul>
            				<label class="radio"><input type="radio" name="category" id="id_category_2" value="sc" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="category" data-parsley-id="6472">SC</label>
            				<label class="radio"><input type="radio" name="category" id="id_category_3" value="st" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="category" data-parsley-id="6472">ST</label>
            				<label class="radio"><input type="radio" name="category" id="id_category_4" value="obc" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="category" data-parsley-id="6472">OBC</label>
            			</div>
            		</div>
            		<div id="div_id_pd" class="form-group form-inline">
            			<label for="id_pd_0" class="control-label col-lg-2 requiredField">Physically Disabled<span class="asteriskField">*</span></label>
            			<div class="controls col-lg-4">
            				<label class="radio"><input type="radio" name="pd" id="id_pd_1" value="yes" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="pd" data-parsley-id="8091">Yes</label>
            				<ul class="parsley-errors-list" id="parsley-id-multiple-pd"></ul>
            				<label class="radio"><input type="radio" name="pd" id="id_pd_2" value="no" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="pd" data-parsley-id="8091">No</label>
            			</div>
            		</div>
            		<div id="div_id_dob" class="form-group">
            			<label for="id_dob" class="control-label col-lg-2 requiredField">Date Of Birth<span class="asteriskField">*</span></label>
            			<div class="controls col-lg-4">
            				<div class="input-group date" id="id_dob_picker">
            					<input type='date' min="1980-01-01" name="dob" readonly/>
            					<!--<input class="datepicker form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_dob" name="dob" data-parsley-id="6216 /">-->
            					<ul class="parsley-errors-list" id="parsley-id-6216"></ul> 
            				</div>
            				<p id="hint_id_dob" class="help-block">Please use the following format: <em>YYYY-MM-DD</em>.</p>
                        </div>
                    </div>
                    <div id="div_id_warchildren" class="form-group form-inline">
                    	<label for="id_warchildren_0" class="control-label col-lg-2 requiredField">Children/War Widows of Defence Personnel killed/Disabled in Action<span class="asteriskField">*</span></label>
                    	<div class="controls col-lg-4"><label class="radio">
                    		<input type="radio" name="warchildren" id="id_warchildren_1" value="yes" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="warchildren" data-parsley-id="0235">Yes</label>
                    		<ul class="parsley-errors-list" id="parsley-id-multiple-warchildren"></ul>
                    		<label class="radio"><input type="radio" name="warchildren" id="id_warchildren_2" value="no" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-multiple="warchildren" data-parsley-id="0235">No</label>
                       	</div>
                    </div>
                    <div id="div_id_fathersname" class="form-group">
                    	<label for="id_fathersname" class="control-label col-lg-2 requiredField">Father's Name<span class="asteriskField">*</span></label>
                    	<div class="controls col-lg-4">
                    		<input class="textinput textInput form-control" data-parsley-maxlength="128" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_fathersname" maxlength="128" name="fathersname" type="text" data-parsley-id="6407">
                    		<ul class="parsley-errors-list" id="parsley-id-6407"></ul>
                    	 </div>
                   	</div>
                   	<div id="div_id_nationality" class="form-group">
                   		<label for="id_nationality" class="control-label col-lg-2 requiredField">Nationality<span class="asteriskField">*</span></label>
                		<div class="controls col-lg-4">
                			<select id="id_nationality" class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." name="nationality" data-parsley-id="6315">
                			<option value="" selected="selected">---------</option>
                			<!-- option value="afghan">Afghan</option -->
                			<!--  option value="zimbabwean">Zimbabwean</option -->
                			</select>
                			<script language="javascript">
                				var nations = ["Afghan", "Albanian", "Algerian",
                				   			"American", "Andorran", "Angolan", "Antiguans", "Argentinean", "Armenian", "Australian", "Austrian",
                							"Azerbaijani", "Bahamian", "Bahraini", "Bangladeshi", "Barbadian", "Barbudans", "Batswana", "Belarusian",
                							"Belgian", "Belizean", "Beninese", "Bhutanese", "Bolivian", "Bosnian", "Brazilian", "British", "Bruneian",
                							"Bulgarian", "Burkinabe", "Burmese", "Burundian", "Cambodian", "Cameroonian", "Canadian", "Cape Verdean",
                							"Central African", "Chadian", "Chilean", "Chinese", "Colombian", "Comoran", "Congolese", "Costa Rican",
                							"Croatian", "Cuban", "Cypriot", "Czech", "Danish", "Djibouti", "Dominican", "Dutch", "East Timorese",
                							"Ecuadorean", "Egyptian", "Emirian", "Equatorial Guinean", "Eritrean", "Estonian", "Ethiopian", "Fijian",
                							"Filipino", "Finnish", "French", "Gabonese", "Gambian", "Georgian", "German", "Ghanaian", "Greek",
                							"Grenadian", "Guatemalan", "Guinea-Bissauan", "Guinean", "Guyanese", "Haitian", "Herzegovinian", "Honduran",
                							"Hungarian", "I-Kiribati", "Icelander", "Indian", "Indonesian", "Iranian", "Iraqi", "Irish", "Israeli",
                							"Italian", "Ivorian", "Jamaican", "Japanese", "Jordanian", "Kazakhstani", "Kenyan", "Kittian and Nevisian",
                							"Kuwaiti", "Kyrgyz", "Laotian", "Latvian", "Lebanese", "Liberian", "Libyan", "Liechtensteiner",
                							"Lithuanian", "Luxembourger", "Macedonian", "Malagasy", "Malawian", "Malaysian", "Maldivan", "Malian",
                							"Maltese", "Marshallese", "Mauritanian", "Mauritian", "Mexican", "Micronesian", "Moldovan", "Monacan",
                							"Mongolian", "Moroccan", "Mosotho", "Motswana", "Mozambican", "Namibian", "Nauruan", "Nepalese",
                							"New Zealander", "Ni-Vanuatu", "Nicaraguan", "Nigerian", "Nigerien", "North Korean", "Northern Irish",
                							"Norwegian", "Omani", "Pakistani", "Palauan", "Panamanian", "Papua New Guinean", "Paraguayan", "Peruvian",
                							"Polish", "Portuguese", "Qatari", "Romanian", "Russian", "Rwandan", "Saint Lucian", "Salvadoran", "Samoan",
                							"San Marinese", "Sao Tomean", "Saudi", "Scottish", "Senegalese", "Serbian", "Seychellois", "Sierra Leonean",
                							"Singaporean", "Slovakian", "Slovenian", "Solomon Islander", "Somali", "South African", "South Korean",
                							"Spanish", "Sri Lankan", "Sudanese", "Surinamer", "Swazi", "Swedish", "Swiss", "Syrian", "Taiwanese",
                							"Tajik", "Tanzanian", "Thai", "Togolese", "Tongan", "Trinidadian or Tobagonian", "Tunisian", "Turkish",
                							"Tuvaluan", "Ugandan", "Ukrainian", "Uruguayan", "Uzbekistani", "Venezuelan", "Vietnamese", "Welsh",
                							"Yemenite", "Zambian", "Zimbabwean"];
                				var nat = document.getElementById("id_nationality");
                				for(var i = 0; i < nations.length; i++) {
    							    var opt = nations[i];
    							    var el = document.createElement("option");
    							    el.textContent = opt;
    							    el.value = opt;
    							    
    							    nat.appendChild(el);
                				}   							
                			</script>
                			<ul class="parsley-errors-list" id="parsley-id-6315"></ul>
                		</div>
                	</div>
                	<div id="div_id_address1" class="form-group">
                		<label for="id_address1" class="control-label col-lg-2 requiredField">Permanent Address<span class="asteriskField">*</span></label>
                		<div class="controls col-lg-4">
                			<textarea class="textarea form-control" cols="40" data-parsley-maxlength="512" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_address1" maxlength="512" name="address1" rows="4" data-parsley-id="8186"></textarea>
                			<ul class="parsley-errors-list" id="parsley-id-8186"></ul>
                		</div>
                	</div>
                	<div id="div_id_pincode" class="form-group">
                		<label for="id_pincode" class="control-label col-lg-2">Pin Code</label>
                		<div class="controls col-lg-4">
                			<input class="numberinput form-control" data-parsley-type="digits" data-parsley-type-digits-message="Enter a whole number." id="id_pincode" name="pincode" type="number" data-parsley-id="8336">
                			<ul class="parsley-errors-list" id="parsley-id-8336"></ul>
                		</div>
                	</div>
                	<!-- input type="submit" name="per" value="save" class="btn btn-primary" id="submit-id-per" onclick="enableSubmit1();"--> 
            	<!-- /form-->
            	</div>
            	
					
					
					
					<div class="tab-pane fade " id="einfo">
    				<!-- form method="POST" id="form2" class="form-horizontal" role="form" enctype="multipart/form-data" novalidate=""-->
    				<fieldset>
    				<legend>Schooling Information</legend>
    				
    					<div id="div_id_x_board" class="form-group">
    						<label for="id_x_board" class="control-label col-lg-2 requiredField">Xth Board<span class="asteriskField">*</span></label>
    						<div class="controls col-lg-4">
    							<input class="textinput textInput form-control" data-parsley-maxlength="64" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_x_board" maxlength="64" name="x_board" type="text" data-parsley-id="2816">
    							<ul class="parsley-errors-list" id="parsley-id-2816"></ul> 
    						</div>
    					</div>
    					<div id="div_id_x_marks" class="form-group">
    						<label for="id_x_marks" class="control-label col-lg-2 requiredField">Xth Marks (%)<span class="asteriskField">*</span></label>
    						<div class="controls col-lg-4">
    							<input class="numberinput form-control" data-parsley-max="100.0" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_x_marks" max="100.0" min="0.0" name="x_marks" step="any" type="number" data-parsley-id="3077">
    							<ul class="parsley-errors-list" id="parsley-id-3077"></ul> 
    						</div>
    					</div>
    					<div id="div_id_x_year" class="form-group">
    						<label for="id_x_year" class="control-label col-lg-2 requiredField">Year of Passing Xth<span class="asteriskField">*</span></label>
    						<div class="controls col-lg-4">
    							<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_x_year" name="x_year" data-parsley-id="5939">
    								<option value="2013" selected="selected">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option>
    								<option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option>
    								<option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option>
    								<option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option>
    								<option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option>
    								<option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option>
    								<option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option>
    								<option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option>
    							</select>
    							<ul class="parsley-errors-list" id="parsley-id-5939"></ul>
    						</div>
    					</div>
    					<div id="div_id_xii_board" class="form-group">
    						<label for="id_xii_board" class="control-label col-lg-2 requiredField">XIIth Board<span class="asteriskField">*</span></label>
    						<div class="controls col-lg-4">
    							<input class="textinput textInput form-control" data-parsley-maxlength="64" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_xii_board" maxlength="64" name="xii_board" type="text" data-parsley-id="4937">
    							<ul class="parsley-errors-list" id="parsley-id-4937"></ul> 
    						</div>
    					</div>
    					<div id="div_id_xii_marks" class="form-group">
    						<label for="id_xii_marks" class="control-label col-lg-2 requiredField">XIIth Marks (%)<span class="asteriskField">*</span></label>
    						<div class="controls col-lg-4">
    							<input class="numberinput form-control" data-parsley-max="100.0" data-parsley-required="true" data-parsley-required-message="This field is required." data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_xii_marks" max="100.0" min="0.0" name="xii_marks" step="any" type="number" data-parsley-id="5262">
    							<ul class="parsley-errors-list" id="parsley-id-5262"></ul> 
    						</div>
    					</div>
    					<div id="div_id_xii_year" class="form-group">
		    				<label for="id_xii_year" class="control-label col-lg-2 requiredField">Year of Passing XIIth<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
		    					<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_xii_year" name="xii_year" data-parsley-id="4005">
		    					<option value="2015" selected="selected">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option>
		    					<option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option>
		    					<option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option>
		    					<option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option>
		    					<option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option>
		    					<option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option>
		    					<option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option>
		    					<option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option>
		    					<option value="1967">1967</option><option value="1966">1966</option>
		    					</select>
		    					<ul class="parsley-errors-list" id="parsley-id-4005"></ul>
		    				</div>
		    			</div>
		    		</fieldset>
		    		<fieldset id="ugrad_fieldset">
		    			<legend>Graduation Information</legend>
		    			<div id="div_id_u_degree" class="form-group">
		    				<label for="id_u_degree" class="control-label col-lg-2 requiredField">Degree<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
		    					<input class="textinput textInput form-control" data-parsley-maxlength="32" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_degree" maxlength="32" name="u_degree" type="text" data-parsley-id="2996">
		    					<ul class="parsley-errors-list" id="parsley-id-2996"></ul> 
		    				</div>
		    			</div>
		    			<div id="div_id_u_department" class="form-group">
		    				<label for="id_u_department" class="control-label col-lg-2 requiredField">Department/Discipline<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
		    					<input class="textinput textInput form-control" data-parsley-maxlength="128" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_department" maxlength="128" name="u_department" type="text" data-parsley-id="2538">
		    					<ul class="parsley-errors-list" id="parsley-id-2538"></ul> 
		    				</div>
		    			</div>
		    			<div id="div_id_u_college" class="form-group">
		    				<label for="id_u_college" class="control-label col-lg-2 requiredField">Name of College<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
		    					<input class="textinput textInput form-control" data-parsley-maxlength="256" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_college" maxlength="256" name="u_college" type="text" data-parsley-id="6031">
		    					<ul class="parsley-errors-list" id="parsley-id-6031"></ul> 
		    				</div>
		    			</div>
		    			<div id="div_id_u_university" class="form-group">
		    				<label for="id_u_university" class="control-label col-lg-2 requiredField">Name of University<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
		    					<input class="textinput textInput form-control" data-parsley-maxlength="256" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_university" maxlength="256" name="u_university" type="text" data-parsley-id="8805">
		    					<ul class="parsley-errors-list" id="parsley-id-8805"></ul> 
		    				</div>
		    			</div>
		    			<div id="div_id_u_city" class="form-group">
		    				<label for="id_u_city" class="control-label col-lg-2 requiredField">City<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
			    				<input class="textinput textInput form-control" data-parsley-maxlength="32" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_city" maxlength="32" name="u_city" type="text" data-parsley-id="8319">
			    				<ul class="parsley-errors-list" id="parsley-id-8319"></ul> 
		    				</div>
		    			</div>
		    			<div id="div_id_u_state" class="form-group">
		    				<label for="id_u_state" class="control-label col-lg-2 requiredField">State<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
			    				<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_state" name="u_state" data-parsley-id="8220">
			    				<option value="" selected="selected">---------</option>
			    				<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option>
			    				<option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhatisgarh">Chhatisgarh</option><option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option>
			    				<option value="Delhi">Delhi</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option>
			    				<option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Lakshadweep">Lakshadweep</option><option value="Madhya Pradesh">Madhya Pradesh</option>
			    				<option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option>
			    				<option value="Orissa">Orissa</option><option value="Pondicherry">Pondicherry</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option>
			    				<option value="Tripura">Tripura</option><option value="Uttaranchal">Uttaranchal</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="West Bengal">West Bengal</option><option value="Other">Other</option>
			    				</select>
			    				<ul class="parsley-errors-list" id="parsley-id-8220"></ul>
		    				</div>
		    			</div>
		    			<div id="div_id_u_year" class="form-group">
		    				<label for="id_u_year" class="control-label col-lg-2 requiredField">Year of Graduation<span class="asteriskField">*</span></label>
		    				<div class="controls col-lg-4">
		    					<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_u_year" name="u_year" data-parsley-id="3484">
		    					<option value="2015" selected="selected">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option>
		    					<option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option>
		    					<option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option>
		    					<option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option>
		    					<option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option>
		    					<option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option>
		    					<option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option>
		    					<option value="1966">1966</option>
		    					</select>
		    					<ul class="parsley-errors-list" id="parsley-id-3484"></ul>
		    				</div>
		    			</div>
		    			<div class="form-group">
		    				<label for="u_cgpa_marks" class="control-label col-lg-2">CGPA or MARKS?</label>
		    				<div class="col-lg-10 controls">
		    					<input type="radio" name="u_cgpa_marks" value="CGPA" data-parsley-multiple="u_cgpa_marks" data-parsley-id="4791">CGPA		                                    
		    					<div class="form-inline">
		                    		<div id="div_id_u_cgpa" class="form-group">
		                            	<div class="controls col-lg-10">
		                                    <input class="form-control numberinput form-control" data-parsley-max="10.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_u_cgpa" max="4" min="0.0" name="u_cgpa" step="any" type="number" data-parsley-id="0954">
		                                    <ul class="parsley-errors-list" id="parsley-id-0954"></ul> 
		                                </div>
		                            </div>
		                          	<div id="div_id_u_scale" class="form-group">
		                                <label for="id_u_scale" class="control-label col-lg-2">/</label>
		    							<div class="controls col-lg-4">
		    								<select class="form-control select form-control" id="id_u_scale" name="u_scale" data-parsley-id="7702">
		    								<option value="4">4</option><option value="10">10</option>
		    								</select>
		    								<ul class="parsley-errors-list" id="parsley-id-7702"></ul>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    				<ul class="parsley-errors-list" id="parsley-id-multiple-u_cgpa_marks"></ul>
		    				<div class="col-lg-4 col-lg-offset-2">
		    					<input type="radio" name="u_cgpa_marks" value="u_marks" data-parsley-required="true" data-parsley-multiple="u_cgpa_marks" data-parsley-id="4791">MARKS(%)
								<div id="div_id_u_marks" class="form-group">
    								<div class="controls col-lg-12">
    									<input class="numberinput form-control" data-parsley-max="100.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_u_marks" max="100.0" min="0.0" name="u_marks" step="any" type="number" data-parsley-id="7377">
    									<ul class="parsley-errors-list" id="parsley-id-7377"></ul> 
    								</div>
    							</div>
    						</div>
    					</div>
    				</fieldset>
    				
    				
    				
    				<div class="form-group">
    					<div id="div_id_is_ece" class="checkbox">
    						<div class="controls col-lg-offset-2 col-lg-4">
    							<label for="id_is_ece" class="">
    							<input class="checkboxinput checkbox" id="id_is_ece" name="is_ece" value="is_ece" type="checkbox" data-parsley-multiple="is_ece" data-parsley-id="6330" onclick="openEce();" >Are you applying for ECE PhD?</label>
                   				<ul class="parsley-errors-list" id="parsley-id-multiple-is_ece"></ul>
                   			</div>
                   		</div>
                   	</div>
                   	<fieldset class="is_ece">
                   		<legend>ECE PhD Subject Preference</legend>
                 			<p>You are required to select 4 subjects if you are an under-graduate otherwise you need to fill 3. </p>
               				<div id="div_id_if_ece_subject1" class="form-group">
               					<label for="id_if_ece_subject1" class="control-label col-lg-2">Preference 1*</label>
								<div class="controls col-lg-4">
	 								<select class="select form-control" id="id_if_ece_subject1" name="if_ece_subject1" data-parsley-id="6395">
	 								<option value="" selected="selected">---------</option>
	 								<option value="Advanced Signal Processing">Advanced Signal Processing</option><option value="Statistical Signal Processing">Statistical Signal Processing</option><option value="Digital VLSI Design">Digital VLSI Design</option><option value="Analog CMOS design">Analog CMOS design</option><option value="Digital Communications">Digital Communications</option><option value="Communication Networks">Communication Networks</option><option value="Linear systems">Linear systems</option><option value="Introduction to Robotics">Introduction to Robotics</option><option value="RF Circuit design">RF Circuit design</option><option value="Antennas and Propagation">Antennas and Propagation</option><option value="Embedded Systems">Embedded Systems</option>
	 								</select>
	 								<ul class="parsley-errors-list" id="parsley-id-6395"></ul>
								</div>
  							</div>
  							<div id="div_id_if_ece_subject2" class="form-group">
  								<label for="id_if_ece_subject2" class="control-label col-lg-2">Preference 2*</label>
		    				<div class="controls col-lg-4">
		    					<select class="select form-control" id="id_if_ece_subject2" name="if_ece_subject2" data-parsley-id="3727">
		    					<option value="" selected="selected">---------</option>
		    					<option value="Advanced Signal Processing">Advanced Signal Processing</option><option value="Statistical Signal Processing">Statistical Signal Processing</option><option value="Digital VLSI Design">Digital VLSI Design</option><option value="Analog CMOS design">Analog CMOS design</option><option value="Digital Communications">Digital Communications</option><option value="Communication Networks">Communication Networks</option><option value="Linear systems">Linear systems</option><option value="Introduction to Robotics">Introduction to Robotics</option><option value="RF Circuit design">RF Circuit design</option><option value="Antennas and Propagation">Antennas and Propagation</option><option value="Embedded Systems">Embedded Systems</option>
		    					</select>
		    					<ul class="parsley-errors-list" id="parsley-id-3727"></ul>
		    				</div>
		    				</div>
		    				<div id="div_id_if_ece_subject3" class="form-group">
		    					<label for="id_if_ece_subject3" class="control-label col-lg-2">Preference 3*</label>
								<div class="controls col-lg-4">
	  								<select class="select form-control" id="id_if_ece_subject3" name="if_ece_subject3" data-parsley-id="6016">
	  								<option value="" selected="selected">---------</option>
	  								<option value="Advanced Signal Processing">Advanced Signal Processing</option><option value="Statistical Signal Processing">Statistical Signal Processing</option><option value="Digital VLSI Design">Digital VLSI Design</option><option value="Analog CMOS design">Analog CMOS design</option><option value="Digital Communications">Digital Communications</option><option value="Communication Networks">Communication Networks</option><option value="Linear systems">Linear systems</option><option value="Introduction to Robotics">Introduction to Robotics</option><option value="RF Circuit design">RF Circuit design</option><option value="Antennas and Propagation">Antennas and Propagation</option><option value="Embedded Systems">Embedded Systems</option>
	  								</select>
	  								<ul class="parsley-errors-list" id="parsley-id-6016"></ul>
  								</div>
  							</div>
  							<div id="div_id_if_ece_subject4" class="form-group">
  								<label for="id_if_ece_subject4" class="control-label col-lg-2">Preference 4*</label>
  								<div class="controls col-lg-4">
  									<select class="select form-control" id="id_if_ece_subject4" name="if_ece_subject4" data-parsley-id="2283">
  									<option value="" selected="selected">---------</option>
  									<option value="Advanced Signal Processing">Advanced Signal Processing</option><option value="Statistical Signal Processing">Statistical Signal Processing</option><option value="Digital VLSI Design">Digital VLSI Design</option><option value="Analog CMOS design">Analog CMOS design</option><option value="Digital Communications">Digital Communications</option><option value="Communication Networks">Communication Networks</option><option value="Linear systems">Linear systems</option><option value="Introduction to Robotics">Introduction to Robotics</option><option value="RF Circuit design">RF Circuit design</option><option value="Antennas and Propagation">Antennas and Propagation</option><option value="Embedded Systems">Embedded Systems</option>
  									</select>
  									<ul class="parsley-errors-list" id="parsley-id-2283"></ul>
  								</div>
  							</div>
  						</fieldset>
    					<div class="form-group">
    						<div id="div_id_is_pg" class="checkbox">
    							<div class="controls col-lg-offset-2 col-lg-4">
    								<label for="id_is_pg" class="">
    								<input class="checkboxinput checkbox" id="id_is_pg" name="is_pg" value="is_pg" type="checkbox" data-parsley-multiple="is_pg" data-parsley-id="5556"> Have you completed your Post Graduation?</label>
    								<ul class="parsley-errors-list" id="parsley-id-multiple-is_pg"></ul>
    							</div>
    						</div>
    					</div>
    					<fieldset class="is_pg">
    						<legend>Post - Graduation Information</legend>
    						<div id="div_id_p_college" class="form-group">
    							<label for="id_p_college" class="control-label col-lg-2">Name of College</label>
    							<div class="controls col-lg-4">
    								<input class="textinput textInput form-control" data-parsley-maxlength="256" id="id_p_college" maxlength="256" name="p_college" type="text" data-parsley-id="7637">
    								<ul class="parsley-errors-list" id="parsley-id-7637"></ul> 
    							</div>
    						</div>
    						<div id="div_id_p_city" class="form-group">
    							<label for="id_p_city" class="control-label col-lg-2">City</label>
    							<div class="controls col-lg-4">
    								<input class="textinput textInput form-control" data-parsley-maxlength="32" id="id_p_city" maxlength="32" name="p_city" type="text" data-parsley-id="7010">
    								<ul class="parsley-errors-list" id="parsley-id-7010"></ul> 
    							</div>
    						</div>
    						<div id="div_id_p_state" class="form-group">
    							<label for="id_p_state" class="control-label col-lg-2">State</label>
    							<div class="controls col-lg-4">
    							<select class="select form-control" id="id_p_state" name="p_state" data-parsley-id="5919">
    							<option value="" selected="selected">---------</option>
    							<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhatisgarh">Chhatisgarh</option><option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option><option value="Daman and Diu">Daman and Diu</option><option value="Delhi">Delhi</option><option value="Goa">Goa</option><option value="Gujarat">Gujarat</option><option value="Haryana">Haryana</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Jammu and Kashmir">Jammu and Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Lakshadweep">Lakshadweep</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Manipur">Manipur</option><option value="Meghalaya">Meghalaya</option><option value="Mizoram">Mizoram</option><option value="Nagaland">Nagaland</option><option value="Orissa">Orissa</option><option value="Pondicherry">Pondicherry</option><option value="Punjab">Punjab</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Tripura">Tripura</option><option value="Uttaranchal">Uttaranchal</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="West Bengal">West Bengal</option><option value="Other">Other</option>
    							</select>
    							<ul class="parsley-errors-list" id="parsley-id-5919"></ul>
    							</div>
    						</div>
    						<div id="div_id_p_department" class="form-group">
    							<label for="id_p_department" class="control-label col-lg-2">Department/Discipline</label>
    							<div class="controls col-lg-4">
    							<input class="textinput textInput form-control" data-parsley-maxlength="128" id="id_p_department" maxlength="128" name="p_department" type="text" data-parsley-id="0033">
    							<ul class="parsley-errors-list" id="parsley-id-0033"></ul> 
    							</div>
    						</div>
    						<div id="div_id_p_degree" class="form-group">
    							<label for="id_p_degree" class="control-label col-lg-2">Degree</label>
    							<div class="controls col-lg-4">
    								<input class="textinput textInput form-control" data-parsley-maxlength="32" id="id_p_degree" maxlength="32" name="p_degree" type="text" data-parsley-id="0489">
    								<ul class="parsley-errors-list" id="parsley-id-0489"></ul> 
    							</div>
    						</div>
    						<div id="div_id_p_thesis" class="form-group">
    							<label for="id_p_thesis" class="control-label col-lg-2">Thesis Title</label>
    							<div class="controls col-lg-4">
    							<input class="textinput textInput form-control" data-parsley-maxlength="128" id="id_p_thesis" maxlength="128" name="p_thesis" type="text" data-parsley-id="3977">
    							<ul class="parsley-errors-list" id="parsley-id-3977"></ul> 
    							</div>
    						</div>
    						<div id="div_id_p_year" class="form-group">
    							<label for="id_p_year" class="control-label col-lg-2 requiredField">Year of Post-Graduation<span class="asteriskField">*</span></label>\
    							<div class="controls col-lg-4">
    								<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_p_year" name="p_year" data-parsley-id="8048">
    								<option value="2015" selected="selected">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option>
    								</select>
    								<ul class="parsley-errors-list" id="parsley-id-8048"></ul>
    							</div>
    						</div>
    						<div class="form-group">
	    						<label for="p_cgpa_marks" class="control-label col-lg-2">CGPA or MARKS?</label>
	    						<div class="col-lg-10 controls">
	    							<input type="radio" name="p_cgpa_marks" id="p_cgpa_radio" value="CGPA" data-parsley-multiple="p_cgpa_marks" data-parsley-id="7668">CGPA
                                <div class="form-inline">
                                	<div id="div_id_p_cgpa" class="form-group">
                                		<div class="controls col-lg-10">
                                		<input class="form-control numberinput form-control" data-parsley-max="10.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_p_cgpa" max="4" min="0.0" name="p_cgpa" step="any" type="number" data-parsley-id="7815">
                                		<ul class="parsley-errors-list" id="parsley-id-7815"></ul> 
                                		</div>
                                	</div>
                                <div id="div_id_p_scale" class="form-group">
	                                <label for="id_p_scale" class="control-label col-lg-2">/</label>
	                                <div class="controls col-lg-4">
	                                	<select class="form-control select form-control" id="id_p_scale" name="p_scale" data-parsley-id="3891">
	                                	<option value="4">4</option><option value="10">10</option>
	                                	</select>
	                                	<ul class="parsley-errors-list" id="parsley-id-3891"></ul>
	                                </div>
                               </div>
                          </div>
                     </div>
                     <ul class="parsley-errors-list" id="parsley-id-multiple-p_cgpa_marks"></ul>
                     <div class="col-lg-4 col-lg-offset-2">
                     	<input type="radio" name="p_cgpa_marks" id="p_marks_radio" value="p_marks" data-parsley-multiple="p_cgpa_marks" data-parsley-id="7668">MARKS(%)
        		    	<div id="div_id_p_marks" class="form-group">
        		    		<div class="controls col-lg-12">
        		    		<input class="numberinput form-control" data-parsley-max="100.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_p_marks" max="100.0" min="0.0" name="p_marks" step="any" type="number" data-parsley-id="9668">
        		    		<ul class="parsley-errors-list" id="parsley-id-9668"></ul> 
        		    		</div>
        		    	</div>
        		    </div>
        		</div>
        		</fieldset>
        		<div class="form-group">
        	    	<div id="div_id_is_other" class="checkbox">
        		    	<div class="controls col-lg-offset-2 col-lg-4">
        			    	<label for="id_is_other" class="">
        			    	<input class="checkboxinput checkbox" id="id_is_other" name="is_other" type="checkbox" data-parsley-multiple="is_other" data-parsley-id="5357">  Other Academic Degrees?
                            </label>
                            <ul class="parsley-errors-list" id="parsley-id-multiple-is_other"></ul>
                        </div>
                    </div>
                </div>
                <fieldset class="is_other">
                	<legend>Other Academic Degrees</legend>
                	<div id="div_id_other_exam" class="form-group">
                		<label for="id_other_exam" class="control-label col-lg-2">Exam Name</label>
    					<div class="controls col-lg-4">
    					<input class="textinput textInput form-control" data-parsley-maxlength="32" id="id_other_exam" maxlength="32" name="other_exam" type="text" data-parsley-id="2719">
    					<ul class="parsley-errors-list" id="parsley-id-2719"></ul> 
    					</div>
    				</div>
    				<div id="div_id_other_subject" class="form-group">
    					<label for="id_other_subject" class="control-label col-lg-2">Subject</label>
    					<div class="controls col-lg-4">
    					<input class="textinput textInput form-control" data-parsley-maxlength="64" id="id_other_subject" maxlength="64" name="other_subject" type="text" data-parsley-id="9669">
    					<ul class="parsley-errors-list" id="parsley-id-9669"></ul> 
    					</div>
    				</div>
    				<div id="div_id_other_year" class="form-group">
    					<label for="id_other_year" class="control-label col-lg-2 requiredField">Year<span class="asteriskField">*</span></label>
    					<div class="controls col-lg-4">
    					<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_other_year" name="other_year" data-parsley-id="8713">
    					<option value="2015" selected="selected">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option>
    					</select>
    					<ul class="parsley-errors-list" id="parsley-id-8713"></ul>
    					</div>
    				</div>
    				<div id="div_id_other_score" class="form-group">
    					<label for="id_other_score" class="control-label col-lg-2">Score</label>
    					<div class="controls col-lg-4">
    					<input class="numberinput form-control" data-parsley-max="100.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_other_score" max="100.0" min="0.0" name="other_score" step="any" type="number" data-parsley-id="3384">
    					<ul class="parsley-errors-list" id="parsley-id-3384"></ul> 
    					</div>
    				</div>
    				<div id="div_id_other_rank" class="form-group">
    					<label for="id_other_rank" class="control-label col-lg-2">Rank</label>
    					<div class="controls col-lg-4">
    					<input class="numberinput form-control" data-parsley-type="digits" data-parsley-type-digits-message="Enter a whole number." id="id_other_rank" name="other_rank" type="number" data-parsley-id="8263">
    					<ul class="parsley-errors-list" id="parsley-id-8263"></ul>
    			 		</div>
    			 	</div>
    			 </fieldset>
    			<div class="form-group">
    				<div id="div_id_is_gate" class="checkbox">
    					<div class="controls col-lg-offset-2 col-lg-4">
    						<label for="id_is_gate" class="">
    						<input class="checkboxinput checkbox" id="id_is_gate" name="is_gate" type="checkbox" data-parsley-multiple="is_gate" data-parsley-id="0778">Taken GATE Exam? </label>
    						<ul class="parsley-errors-list" id="parsley-id-multiple-is_gate"></ul>
    					</div>
    				</div>
    			</div>
                <fieldset class="is_gate">
                <legend>Gate</legend>
                <div id="div_id_gate_area" class="form-group">
                    <label for="id_gate_area" class="control-label col-lg-2">Area</label>
                    <div class="controls col-lg-4">
                    <input class="textinput textInput form-control" data-parsley-maxlength="128" id="id_gate_area" maxlength="128" name="gate_area" type="text" data-parsley-id="4431">
                    <ul class="parsley-errors-list" id="parsley-id-4431"></ul> 
                    </div>
                </div>
                <div id="div_id_gate_year" class="form-group">
                	<label for="id_gate_year" class="control-label col-lg-2 requiredField">Year of Graduation<span class="asteriskField">*</span></label>
                	<div class="controls col-lg-4">
                	<select class="select form-control" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_gate_year" name="gate_year" data-parsley-id="6431">
                	<option value="2015" selected="selected">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option>
                	</select>
                	<ul class="parsley-errors-list" id="parsley-id-6431"></ul>
                	</div>
                </div>
                <div id="div_id_gate_marks" class="form-group">
                	<label for="id_gate_marks" class="control-label col-lg-2">Marks(Out Of 100)</label>
                	<div class="controls col-lg-4">
                	<input class="numberinput form-control" data-parsley-max="100.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_gate_marks" max="100.0" min="0.0" name="gate_marks" step="any" type="number" data-parsley-id="2728">
                	<ul class="parsley-errors-list" id="parsley-id-2728"></ul> 
                	</div>
                </div>
                <div id="div_id_gate_score" class="form-group">
                	<label for="id_gate_score" class="control-label col-lg-2">Score</label>
                	<div class="controls col-lg-4">
                	<input class="numberinput form-control" data-parsley-max="1000.0" data-parsley-type="number" data-parsley-type-digits-message="Enter a number." data-parsley-type-number-message="Enter a number." id="id_gate_score" max="1000.0" min="0.0" name="gate_score" step="any" type="number" data-parsley-id="8952">
                	<ul class="parsley-errors-list" id="parsley-id-8952"></ul> 
                	</div>
                </div>
                <div id="div_id_gate_rank" class="form-group">
                	<label for="id_gate_rank" class="control-label col-lg-2">Rank</label>
                	<div class="controls col-lg-4">
                		<input class="numberinput form-control" data-parsley-min="1.0" data-parsley-type="digits" data-parsley-type-digits-message="Enter a whole number." id="id_gate_rank" min="1.0" name="gate_rank" type="number" data-parsley-id="3139">
                		<ul class="parsley-errors-list" id="parsley-id-3139"></ul> 
                	</div>
                </div>
    		</fieldset>
    	<fieldset>
    	<legend>Achievements, CV and Statement of Purpose</legend>
    	<div id="div_id_achievements" class="form-group">
    		<label for="id_achievements" class="control-label col-lg-2">Achievements (Other information like ranks, medals etc.)</label>
    		<div class="controls col-lg-4">
    			<input class="textinput textInput form-control" data-parsley-maxlength="500" id="id_achievements" maxlength="500" name="achievements" type="text" data-parsley-id="7157">
    			<ul class="parsley-errors-list" id="parsley-id-7157"></ul> 
    		</div>
    	</div>
    	<div id="div_id_path" class="form-group">
    		<label for="id_path" class="control-label col-lg-2 requiredField">CV/Resume<span class="asteriskField">*</span></label>
    		<div class="controls col-lg-4">
    		<input class="clearablefileinput" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_path" name="path" type="file" data-parsley-id="2910">
    		<ul class="parsley-errors-list" id="parsley-id-2910"></ul> 
    		<p id="hint_id_path" class="help-block">Upload a pdf file of max 10 MB</p>
    		</div>
    	</div>
    	<div id="div_id_sop_path" class="form-group">
    		<label for="id_sop_path" class="control-label col-lg-2 requiredField">Statement of Purpose<span class="asteriskField">*</span></label>
    		<div class="controls col-lg-4">
    			<input class="clearablefileinput" data-parsley-required="true" data-parsley-required-message="This field is required." id="id_sop_path" name="sop_path" type="file" data-parsley-id="9645">
    			<ul class="parsley-errors-list" id="parsley-id-9645"></ul> 
    			<p id="hint_id_sop_path" class="help-block">Upload a pdf file of max 10 MB</p>
    		</div>
   		</div>
   		<!--input type="button" name="edu" value="save" class="btn btn-primary" id="submit-id-edu" onclick="enableSubmit2();"-->
   		</fieldset>
    	<!--/form-->
    			
  		</div>
    	        
    	<div class="tab-pane fade" id="submit">
  			<p>Please complete and save all the fields before you click the submit button. Once you have submitted you will not be able to edit any field.</p>
  			<div id="button_submit" style="margin-top: 10px" data-toggle="tooltip" title="Please fill all the fields">
    			<button type="submit" id="submit_button" name="all" value="submit" class="btn btn-default">Submit</button>
    		</div>
    	</div>
    		
    </div>
 </form>
</div>
</div>
        
</body>
</html>