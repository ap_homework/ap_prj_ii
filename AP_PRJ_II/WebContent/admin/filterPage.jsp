<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import ="java.util.*" %>
<%@ page import ="db.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
  
<title>PhDFilter</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="page-header">
	<h1>Filtered Records</h1>
</div>
<div class="container">
<div class="jumbotron">
<%! Vector<ExtraClasses> v; %>
<% v = (Vector<ExtraClasses>)request.getAttribute("filteredData"); %>
  <table class="table table-hover row">
    <thead>
      <tr>
        <th class="col-sm-3" ><h4>ENROLLMENT ID</h4></th>
        <th class="col-sm-3" ><h4>NAME</h4></th>
        <th class="col-sm-6">
        	<table>
        	<tr ><td colspan="3" align="center"><h4>LINKS TO DATA</h4></td></tr>
        	<tr>
        	<td class="col-sm-2">SOP</td>
        	<td class="col-sm-2">CV </td>
        	<td class="col-sm-2">LINK TO DATA</td>
        	</tr>
        	</table>
        </th>
        
      </tr>
    </thead>
    <tbody>
      <%for( ExtraClasses e: v){ %>
      <tr>
        <td class="col-sm-3"><%=e.getP().getEnroll()%></td>
        <td class="col-sm-3"><%=e.getP().getName()%></td>
        <td class="col-sm-6">
			<table>
        	<tr>
        	<td class="col-sm-2"><a href="#<%=e.getE().getSop_name() %>" class="btn btn-info" role="button">SOP</a></td>
        	<td class="col-sm-2"><a href="#<%=e.getE().getCv_name() %>" class="btn btn-info" role="button">CV</a></td>
        	<td class="col-sm-2"><a href="#<%=e.getE().getTxt_name()%>" class="btn btn-info" role="button">INFO</a></td>
        	</tr>
        	</table>
		</td>
      </tr>
      <%} %>
    </tbody>
  </table>
</div>
</div>

</body>
</html>