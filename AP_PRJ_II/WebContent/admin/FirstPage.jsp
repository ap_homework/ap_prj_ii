<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import ="java.util.*" %>
<%@ page import ="db.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
  
<title>PhDFilter</title>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="page-header">
	<h1>Filter Entries</h1>
</div>
<div class="container-fluid">	
<div class="jumbotron">
	<ul class = 'nav nav-pills'>
    	<li class= 'active'><a data-toggle="pill"  href="#pinfo">Personal Information</a></li>
    	<li><a data-toggle="pill"  href="#einfo">Educational Information</a></li>
    	<li><a data-toggle="pill" href="#submit">Submit</a></li>
  	</ul>
  	<form action="FilterResult" name="personal_Info" class="form-horizontal">   
	    
  	<div class="tab-content">
	  <div id="pinfo" class="tab-pane fade in active">
		  <fieldset>
	      <legend>Personal Information</legend>
	      <div class="form-group">
	        <label for="email" class="col-sm-2 control-label">Email:</label>
	          <div class="col-sm-4">
	            <input name="email" class="form-control">
	          </div>
	      </div>
	      <div class="form-group">
	        <label for="name" class="col-sm-2 control-label">Name: </label>
	          <div class="col-sm-4">
	            <input name="name" class="form-control">
	          </div>
	      </div>	
	      <div class="form-group">
	        <label for="enroll" class="col-sm-2 control-label">Enroll: </label>
	          <div class="col-sm-4">
	            <input name="enroll" class="form-control">
	          </div>
	      </div>
	      <div class="form-group">
	        <label for="categ" class="col-sm-2 control-label">Category:</label>
		    	<div class="col-sm-4">
			    	<select name= 'categ' class="input-sm">
			    			<option value='-' selected="selected">-</option>
							<option value='General'>General</option>
							<option value='SC'>SC</option>
							<option value='ST'>ST</option>
							<option value='OBC'>OBC</option> 
					</select>
	          	</div>
	      </div>
	      <div class="form-group">
	        <label for="gender" class="col-sm-2 control-label">Gender: </label>
	          <div class="col-sm-6">
	          	<input type='radio' name='gender' value='Male' class="radio-inline"/>Male
				<input type='radio' name='gender' value='Female' class='radio-inline'/>Female
	          </div>
	      </div>
		  <div class="form-group">
	        <label for="pwd" class="col-sm-2 control-label">Physically Disabled: </label>
	          <div class="col-sm-6">
	          	<input type='radio' name='pwd' value='Yes' class='radio-inline'/>Yes
				<input type='radio' name='pwd' value='No' class='radio-inline'/>No
	          </div>
	      </div>
	      <div class="form-group">
	        <label for="dob" class="col-sm-2 control-label">Date Of Birth: </label>
	          <div class="col-sm-8">
	          	<input type='radio' name='dob' value='Before' class='radio-inline'/>Before
				<input type='radio' name='dob' value='On' class='radio-inline'/>On
				<input type='radio' name='dob' value='After'class='radio-inline'/>After 
	          </div>
	      </div>	
	      <div class="form-group">
	      	<label class='col-sm-2'></label>
	          <div class="col-sm-8">
	      		<input type='date' name='DOB' min="1980-01-01" readonly>
	          </div>
	      </div>
	      </fieldset>
	  </div>
   
	  <div id="einfo" class="tab-pane fade in">
      
        <fieldset>
        <legend>Educational Information</legend>
	    <%! String[] states = new String[36];
	    ArrayList<String> graddeg, pgdeg, xboard, xiiboard, graddep, pgdep; 
	    %>
		<%states = (String[]) request.getAttribute("states");
		graddeg = (ArrayList<String>) request.getAttribute("graddeg");
		pgdeg = (ArrayList<String>) request.getAttribute("pgdeg");
		xboard = (ArrayList<String>) request.getAttribute("xboard");
		xiiboard = (ArrayList<String>) request.getAttribute("xiiboard");
		graddep = (ArrayList<String>) request.getAttribute("graddep");
		pgdep = (ArrayList<String>) request.getAttribute("pgdep");
		%>
				  
	      <div class="form-group">
	        <label for="phdStream" class="col-sm-2 control-label">PhD Stream:</label>
		    	<div class="col-sm-4">
			    	<select name= 'phdStream' class="input-sm">
			    			<option value='-' selected='selected'>-</option>
			    			<option value='Computer Science'>Computer Science</option>
			    			<option value='Electronics and Communication'>Electronics and Communication</option>
			    			<option value='Computational Biology'>Computational Biology</option>			    			
					</select>
	          	</div>
	      </div>   
	      <div class="form-group">
	        <label for="gradDeg" class="col-sm-2 control-label">Graduation Degree:</label>
		    	<div class="col-sm-4">
			    	<select name= 'gradDeg' class="input-sm">
			    		<option value="-">-</option>
			    		<%for(String s: graddeg){
					 	  %><option value="<%=s%>"><%=s%></option>
					  	<%} %>	
					</select>
	          	</div>
	      </div>   
	      <div class="form-group">
	        <label for="postGradDeg" class="col-sm-2 control-label">Post Graduation Degree:</label>
		    	<div class="col-sm-4">
			    	<select name= 'postGradDeg' class="input-sm">
			    		<option value="-">-<option>
			    		<%for(String s: pgdeg){
					 	  %><option value="<%=s%>"><%=s%></option>
					  	<%} %>	
					</select>
	          	</div>
	      </div>	      
	      <div class="form-group">
	        <label for="Xboard" class="col-sm-2 control-label">Class Xth Board:</label>
		    	<div class="col-sm-4">
			    	<select name= 'Xboard' class="input-sm">
			    		<option value="-">-</option>
			    		<%for(String s: xboard){
					 	  %><option value="<%=s%>"><%=s%></option>
					  	<%} %>
					</select>
	          	</div>
	      </div>	      
	      <div class="form-group">
	        <label for="XIIboard" class="col-sm-2 control-label">Class XIIth Board:</label>
		    	<div class="col-sm-4">
			    	<select name= 'XIIboard' class="input-sm">
			    		<option value="-">-</option>
			    		<%for(String s: xiiboard){
					 	  %><option value="<%=s%>"><%=s%></option>
					  	<%} %>
					</select>
	          	</div>
	      </div>	      
	      <div class="form-group">
	        <label for="gradDep" class="col-sm-2 control-label">Department(Graduation):</label>
		    	<div class="col-sm-4">
			    	<select name= 'gradDep' class="input-sm">
			    		<option value="-">-</option>
			    		<%for(String s: graddep){
					 	  %><option value="<%=s%>"><%=s%></option>
					  	<%} %>
					</select>
	          	</div>
	      </div>	      
	      <div class="form-group">
	        <label for="postGradDep" class="col-sm-2 control-label">Department(Post Graduation):</label>
		    	<div class="col-sm-4">
			    	<select name= 'postGradDep' class="input-sm">
			    		<option value="-">-</option>
			    		<%for(String s: pgdep){
					 	  %><option value="<%=s%>"><%=s%></option>
					  	<%} %>
					</select>
	          	</div>
	      </div>	      
	      <div class="form-group">
	        <label for="univGrad" class="col-sm-2 control-label">University(Graduation): </label>
	          <div class="col-sm-4">
	            <input name="univGrad" class="form-control">
	          </div>
	      </div>	
	      <div class="form-group">
	        <label for="univPostGrad" class="col-sm-2 control-label">University(Post Graduation): </label>
	          <div class="col-sm-4">
	            <input name="univPostGrad" class="form-control">
	          </div>
	      </div>	
	      <div class="form-group">
	        <label for="stateGrad" class="col-sm-2 control-label">State(Graduation From):</label>
		    	<div class="col-sm-4">
			    	<select name='stateGrad' class='input-sm'>
			    		<option value="-" selected='selected'>-</option>
					  <%for(String s: states){
					 	 %><option value="<%=s%>"><%=s%></option>
					  <%} %>
					</select>

	          	</div>
	      </div>      
	      <div class="form-group">
	        <label for="statePostGrad" class="col-sm-2 control-label">State(Post Graduation From):</label>
		    	<div class="col-sm-4">
			    	<select name= 'statePostGrad' class="input-sm">
			    		<option value='-' selected='selected'>-</option>
			    	  	<%for(String s: states){
					 		 %><option value="<%=s%>"><%=s%></option>
					  	<%} %>		
					</select>
	          	</div>
	      </div>
	      <div class="form-group">
	      	<label for="Xper" class="col-sm-2 control-label">Class X Board Percentage</label>
	     		<div class="col-sm-6">
	     		 		<label><input name="Xper1" onclick="function();"value='Greater Than' type="checkbox" class="checkbox-inline"> Greater Than</label>
        				<label><input name="Xper2" value='Equals' type="checkbox" class="checkbox-inline"> Equals</label>
        				<label><input name="Xper3" value='Lesser Than' type="checkbox" class="checkbox-inline"> Lesser Than</label>
        				<div class = 'col-sm-3'>
        				<input name="Xper" class="form-control" placeholder="Percent" max="100.0" min="0.0" step="any" type="number">
	     				</div>
	     				
	     		</div>
	      </div>	      
	      
	      <div class="form-group">
	      	<label for="XIIper" class="col-sm-2 control-label">Class XII Board Percentage</label>
	     		<div class="col-sm-6">
	     		 		<label><input name="XIIper1" type="checkbox" class="checkbox-inline" value='Greater Than'> Greater Than</label>
        				<label><input name="XIIper2" type="checkbox" class="checkbox-inline" value='Equals'> Equals</label>
        				<label><input name="XIIper3" type="checkbox" class="checkbox-inline" value='Lesser Than'>Lesser Than</label>
	     				<div class=col-sm-3>
	     				<input name="XIIper" class="form-control" placeholder="Percent" max="100.0" min="0.0" step="any" type="number">
	     				</div>
	     		</div>
	      </div>	      
	      <div class="form-group">
	      	<label for="Gradper" class="col-sm-2 control-label">Graduation Marks Percentage</label>
	     		<div class="col-sm-6">
	     		 		<label><input name="Gradper1" type="checkbox" class="checkbox-inline" value='Greater Than'> Greater Than</label>
        				<label><input name="Gradper2" type="checkbox" class="checkbox-inline" value="Equals"> Equals</label>
        				<label><input name="Gradper3" type="checkbox" class="checkbox-inline" value="Lesser Than"> Lesser Than</label>
	     				<div class='col-sm-3'>
	     				<input name="Gradper" class="form-control" placeholder="Percent" max="100.0" min="0.0" step="any" type="number">
	     				</div>
	     		</div>
	      </div>	      
	    <div class="form-group">
	      	<label for="postGradper" class="col-sm-2 control-label">Post Graduation Marks Percentage</label>
	     		<div class="col-sm-6">
	     		 		<label><input name="postGradper1" type="checkbox" class="checkbox-inline" value="Greater Than"> Greater Than</label>
        				<label><input name="postGradper2" type="checkbox" class="checkbox-inline" value="Equals"> Equals</label>
        				<label><input name="postGradper3" type="checkbox" class="checkbox-inline" value="Lesser Than"> Lesser Than</label>
	     				<div class='col-sm-3'>
	     				<input name="postGradper" class="form-control" placeholder="Percent" max="100.0" min="0.0" step="any" type="number">
	     				</div>
	     		</div>
	      </div>	      
	    <div class="form-group">
	      	<label for="GateScore" class="col-sm-2 control-label">Gate Score</label>
	     		<div class="col-sm-6">
	     		 		<label><input name="GateScore1" type="checkbox" class="checkbox-inline" value="Greater Than"> Greater Than</label>
        				<label><input name="GateScore2" type="checkbox" class="checkbox-inline" value="Equals"> Equals</label>
        				<label><input name="GateScore3" type="checkbox" class="checkbox-inline" value="Lesser Than"> Lesser Than</label>
	     				<div class='col-sm-3'>
	     				<input name="GateScore" class="form-control" placeholder="Score" max="100.0" min="0.0" step="any" type="number">
	     				</div>
	     		</div>
	      </div>	      	    
	    </fieldset>
	 </div>
 	  <div id="submit" class="tab-pane fade in">
	    	 <fieldset>
	    	 <legend></legend>
	    	 	<div class="form-group">
	      			<label for='from'class='col-sm-3'>Applications Dated From : </label>
	          		<div class="col-sm-8">
	      				<input type='date' name='from' min="1980-01-01">
	          		</div>
	      		</div>
	      		<div class="form-group">
	      			<label for='upto' class='col-sm-3'>Applications Dated Upto : </label>
	          		<div class="col-sm-8">
	      				<input type='date' name='upto' min="1980-01-01">
	          		</div>
	      		</div>
	      
	    	 <button type="submit" class="btn btn-primary" value="Submit Button">Show Filtered Results</button>
	    	 </fieldset>
	  </div>
    </div>
</form>	  
 	
</div>
</div>      
</body>
</html>