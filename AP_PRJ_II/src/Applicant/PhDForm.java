package Applicant;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.Educational_info;
import db.ExtraClasses;
import db.OtherDeg;
import db.Personal_info;
import db.Pg;
import db.PhDECE;
import db.PhDGATE;
import fileHandler.FileWrite;

/**
 * Servlet implementation class PhDForm
 */
@WebServlet("/PhDForm")
public class PhDForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	String[] ecepref2 = new String[]{
			"Advanced Signal Processing",
			"Statistical Signal Processing", "Digital VLSI Design", "Analog CMOS design", "Digital Communications",
			"Communication Networks", "Linear systems", "Introduction to Robotics", "RF Circuit design",
			"Antennas and Propagation", "Embedded Systems"	
	};
	
	public PhDForm() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ExtraClasses e = new ExtraClasses();
		/*read personal info*/
		Personal_info p = new Personal_info();
		p.setEmail(request.getParameter("email"));
		p.setName(request.getParameter("name"));
//		p.setEnroll(request.getParameter("enrollment_number"));
		p.setCorraddr(request.getParameter("address"));
		p.setMob(request.getParameter("mobile"));
		p.setStream(request.getParameter("phd_stream"));
		String[] pref = new String[3];
		pref[0] = request.getParameter("phd_area_pref1");
		pref[1] = request.getParameter("phd_area_pref2");
		pref[2] = request.getParameter("phd_area_pref3");
		p.setPreference(pref);
		p.setGender(request.getParameter("gender"));
		p.setCategory(request.getParameter("category"));
		p.setDisabled(request.getParameter("pd"));
		p.setDob(LocalDate.parse(request.getParameter("dob")));
		p.setDefense(request.getParameter("warchildren"));
		p.setFname(request.getParameter("fathersname"));
		p.setNationality(request.getParameter("nationality"));
		p.setPermaddr(request.getParameter("address1"));
		p.setPin(request.getParameter("pincode"));
		e.setP(p);
		
		/*read Educational Info*/
		Educational_info ed = new Educational_info();
		ed.setXth_board(request.getParameter("x_board"));
		ed.setXth_marks(Double.parseDouble(request.getParameter("x_marks")));
		ed.setXth_pass(Integer.parseInt(request.getParameter("x_year")));
		ed.setXiith_board(request.getParameter("xii_board"));
		ed.setXiith_marks(Double.parseDouble(request.getParameter("xii_marks")));
		ed.setXiith_pass(Integer.parseInt(request.getParameter("xii_year")));
		ed.setDegree(request.getParameter("u_degree"));
		ed.setDep(request.getParameter("u_department"));
		ed.setCollege(request.getParameter("u_college"));
		ed.setUniversity(request.getParameter("u_university"));
		ed.setCity(request.getParameter("u_city"));
		ed.setState(request.getParameter("u_state"));
		ed.setGrad_year(Integer.parseInt(request.getParameter("u_year")));
		String cgpa_marks = request.getParameter("u_cgpa_marks");
		if(cgpa_marks.compareTo("CGPA") == 0){
			ed.setTotal(Double.parseDouble(request.getParameter("u_scale")));
			ed.setGrad_GPA(Double.parseDouble(request.getParameter("u_cgpa")));
		}
		else {
			ed.setGrad_marks(Double.parseDouble(request.getParameter("u_marks")));
		}
		ed.setAchievements(request.getParameter("achievements"));
		e.setE(ed);
		
		/*read PhDECE */
		if(request.getParameter("is_ece") != null){
			PhDECE ece = new PhDECE();
			String[] prf = new String[4];
			prf[0] = request.getParameter("if_ece_subject1");
			prf[2] = request.getParameter("if_ece_subject2");
			prf[2] = request.getParameter("if_ece_subject3");
			prf[3] = request.getParameter("if_ece_subject4");
			ece.setEce_preferences(prf);
			e.setPhdece(ece);
		}
		
		/*read Pg*/
		if(request.getParameter("is_pg") != null)
		{
			Pg pg = new Pg();
			pg.setColl(request.getParameter("p_college"));
			pg.setCity(request.getParameter("p_city"));
			pg.setState(request.getParameter("p_state"));
			pg.setDeg(request.getParameter("p_department"));
			pg.setDeg(request.getParameter("p_degree"));
			pg.setTitle(request.getParameter("p_thesis"));
			pg.setYr(Integer.parseInt(request.getParameter("p_year")));
			if(cgpa_marks.compareTo("CGPA") == 0){
				ed.setTotal(Double.parseDouble(request.getParameter("p_scale")));
				ed.setGrad_GPA(Double.parseDouble(request.getParameter("p_cgpa")));
			}
			else {
				ed.setGrad_marks(Double.parseDouble(request.getParameter("p_marks")));
			}
			e.setPg(pg);
		}
		
		/*read OtherDeg*/
		if(request.getParameter("is_other") != null)
		{
			OtherDeg o = new OtherDeg();
			o.setExam(request.getParameter("other_exam"));
			o.setSubj(request.getParameter("other_subject"));
			o.setYr(Integer.parseInt(request.getParameter("other_year")));
			o.setScore(Integer.parseInt(request.getParameter("other_score")));
			o.setRank(Integer.parseInt(request.getParameter("other_exam")));
			e.setO(o);
		}
		/*read PhDGATE */
		if(request.getParameter("is_gate") != null)
		{
			PhDGATE gate = new PhDGATE();
			gate.setArea(request.getParameter("gate_area"));
			gate.setYear(Integer.parseInt(request.getParameter("gate_year")));
			gate.setMarks(Float.parseFloat(request.getParameter("gate_marks")));
			gate.setScore(Float.parseFloat(request.getParameter("gate_score")));
			gate.setRank(Integer.parseInt(request.getParameter("gate_rank")));
			e.setPhdgate(gate);
		}
		System.out.println(new Date());
		e.setTime_of_applying(new Date());
		
		FileWrite.write_to_file(e);
	}

}
