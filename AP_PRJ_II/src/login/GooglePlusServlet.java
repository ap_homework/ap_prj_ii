package login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.Google2Api;
import org.scribe.oauth.OAuthService;

/**
 * Servlet implementation class GooglePlusServlet
 */
@WebServlet("/GooglePlusServlet")
public class GooglePlusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GooglePlusServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    private static final String CLIENT_ID = "195187413849-6523h0f4p7u5su7ds6oqf8ala89bo6p8.apps.googleusercontent.com"; 
    private static final String CLIENT_SECRET = "WI2RtF5D_stoeWb3hc1TqTGF";
    @Override 
    protected void doGet(HttpServletRequest req, HttpServletResponse res) 
       throws IOException, ServletException {
       //Configure 
       ServiceBuilder builder= new ServiceBuilder(); 
       OAuthService service = builder.provider(Google2Api.class) 
          .apiKey(CLIENT_ID) 
          .apiSecret(CLIENT_SECRET) 
          .callback("http://localhost:8080/AP_PRJ_II/oauth2callback") 
          .scope("openid profile email " + 
                "https://www.googleapis.com/auth/plus.login " + 
                "https://www.googleapis.com/auth/plus.me")  
          .debug() 
          .build(); //Now build the call
       HttpSession sess = req.getSession(); 
       sess.setAttribute("oauth2Service", service);
//       System.out.println("dgadsjkgla  " +service.getAuthorizationUrl(service.getRequestToken()));
//       System.out.println("heehhe "+ service.getAuthorizationUrl(null));
       res.sendRedirect(service.getAuthorizationUrl(null));
       
    } 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
