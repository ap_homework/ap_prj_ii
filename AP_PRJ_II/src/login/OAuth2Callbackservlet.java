package login;

import java.io.IOException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns={"/oauth2callback"}, asyncSupported=true) 

public class OAuth2Callbackservlet extends HttpServlet { /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@Override 
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
      throws IOException, ServletException {
      //Check if the user have rejected 
      String error = req.getParameter("error"); 
      if ((null != error) && ("access_denied".equals(error.trim()))) { 
         HttpSession sess = req.getSession(); 
         sess.invalidate(); 
         resp.sendRedirect(req.getContextPath()); 
         return; 
      }
      AsyncContext ctx = req.startAsync(); 
      ctx.start(new GetUserInfo(req, resp, ctx)); 
      HttpSession sess= req.getSession();
//      System.out.println(sess.getAttribute("email") + "dfdsfsdfadsf");
     String email; 	

     if(sess.getAttribute("email") != null){
    	 email = sess.getAttribute("email").toString();
    	 System.out.println(email);
         if(email.compareTo("sudhanshujaiswal92@gmail.com") == 0  || email.compareTo("harshvardhan14043@iiitd.ac.in") == 0)
        	 req.getRequestDispatcher("/Filter").forward(req, resp);
         else  
        	 req.getRequestDispatcher("Form").forward(req, resp);
//       	  req.getRequestDispatcher("applicant/appl.jsp").forward(req, resp);
     } else  
    	 req.getRequestDispatcher("Form").forward(req, resp);
//    	  req.getRequestDispatcher("applicant/appl.jsp").forward(req, resp);
      
   } 
}