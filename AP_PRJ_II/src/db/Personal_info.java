package db;
//Harshvardhan Kalra,2014043
//Aishwarya Jaiswal,2014007
import java.io.Serializable;
import java.time.LocalDate;

public class Personal_info implements Serializable, Cloneable
{
	private static final long serialVersionUID = 1L;
	String name,email,enroll,corraddr,permaddr,mob,fname,pin, nationality;
	String []preference;
	String gender,category,disabled,stream,defense;
	LocalDate dob;
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}  
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getEnroll() {
		return enroll;
	}
	public void setEnroll(String rollnum) {
		this.enroll = rollnum;
	}
	public String getCorraddr() {
		return corraddr;
	}
	public void setCorraddr(String corraddr) {
		this.corraddr = corraddr;
	}
	public String getPermaddr() {
		return permaddr;
	}
	public void setPermaddr(String permaddr) {
		this.permaddr = permaddr;
	}
	public String getMob() {
		return mob;
	}
	public void setMob(String mob) {
		this.mob = mob;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String[] getPreference() {
		return preference;
	}
	public void setPreference(String[] preference) {
		this.preference = preference;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDisabled() {
		return disabled;
	}
	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
	public String getStream() {
		return stream;
	}
	public void setStream(String stream) {
		this.stream = stream;
	}
	public String getDefense() {
		return defense;
	}
	public void setDefense(String defense) {
		this.defense = defense;
	}
	public LocalDate getDob() {
//		DateTimeFormatter f = DateTimeFormatter.ofPattern("dd-MM-yy");

//		dob.format(f);
		return dob;
	}
	public void setDob(LocalDate dob) {
//		DateTimeFormatter f = DateTimeFormatter.ofPattern("dd-MM-yy");
//		this.dob.format(f);

		this.dob = dob;
	}
}
