package db;
/*Harshvardhan Kalra,2014043
Aishwarya Jaiswal,2014007*/
import java.io.Serializable;
import java.util.Date;

public class ExtraClasses implements Serializable
{
	private static final long serialVersionUID = 1L;
	Personal_info p;
	Educational_info e;
	Pg pg;
	PhDECE phdece;
	OtherDeg o;
	PhDGATE phdgate;
	Date time_of_applying;
	public Date getTime_of_applying() {
		return this.time_of_applying;
	}
	public void setTime_of_applying(Date time_of_applying) {
		this.time_of_applying = time_of_applying;
	}
	public Pg getPg() {
		return pg;
	}
	public void setPg(Pg pg) {
		this.pg = pg;
	}
	public PhDECE getPhdece() {
		return phdece;
	}
	public void setPhdece(PhDECE phdece) {
		this.phdece = phdece;
	}
	public OtherDeg getO() {
		return o;
	}
	public void setO(OtherDeg o) {
		this.o = o;
	}
	public PhDGATE getPhdgate() {
		return phdgate;
	}
	public void setPhdgate(PhDGATE phdgate) {
		this.phdgate = phdgate;
	}
	public Personal_info getP() {
		return p;
	}
	public void setP(Personal_info p) {
		this.p = p;
	}
	public Educational_info getE() {
		return e;
	}
	public void setE(Educational_info e) {
		this.e = e;
	}
}
