package db;

import java.io.Serializable;
import java.util.Vector;

public class DB implements Serializable {
	private static final long serialVersionUID = 1L;
	static Vector<ExtraClasses> applicantRecords = new Vector<ExtraClasses> ();
	private static int currEnrollNumber = -1;	
	public static synchronized int getCurrEnrollNumber() {
		return currEnrollNumber;
	}
	
	public static void setCurrEnrollNumber(int currEnrollNumber) {
		DB.currEnrollNumber = currEnrollNumber;
	}

	public static synchronized void incCurrEnrollNumber() {
		currEnrollNumber++;
	}
	public static Vector<ExtraClasses> getRecoreds()
	{
		return applicantRecords;
	}
	public static void updateRecords(ExtraClasses e)
	{
		applicantRecords.add(e);
	}
}
