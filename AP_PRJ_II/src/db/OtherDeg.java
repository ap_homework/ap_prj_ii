package db;
//Harshvardhan Kalra,2014043
//Aishwarya Jaiswal,2014007
import java.io.Serializable;

public class OtherDeg implements Serializable, Cloneable
{
	private static final long serialVersionUID = 1L;
	int yr,score,rank;
	String exam,subj;
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}
	public int getYr() {
		return yr;
	}
	public void setYr(int yr) {
		this.yr = yr;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getExam() {
		return exam;
	}
	public void setExam(String exam) {
		this.exam = exam;
	}
	public String getSubj() {
		return subj;
	}
	public void setSubj(String subj) {
		this.subj = subj;
	}
}
