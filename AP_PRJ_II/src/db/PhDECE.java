package db;
//Harshvardhan Kalra,2014043
//Aishwarya Jaiswal,2014007
import java.io.Serializable;

public class PhDECE implements Serializable, Cloneable
{
	private static final long serialVersionUID = 1L;
	String []ece_preferences;
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}
	public String[] getEce_preferences() {
		return ece_preferences;
	}

	public void setEce_preferences(String[] ece_preferences) {
		this.ece_preferences = ece_preferences;
	}
}
