package db;
//Harshvardhan Kalra,2014043
//Aishwarya Jaiswal,2014007
import java.io.Serializable;

public class Educational_info implements Cloneable, Serializable
{
	private static final long serialVersionUID = 1L;
	String xth_board,xiith_board,college,dep,degree,university,city,achievements,state;
	double xth_marks,xiith_marks,grad_marks,grad_GPA,total;
	int xth_pass,xiith_pass,grad_year,total_marks;
	String cv_name,sop_name,txt_name; 
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCv_name() {
		return cv_name;
	}
	public void setCv_name(String cv_name) {
		this.cv_name = cv_name;
	}
	public String getSop_name() {
		return sop_name;
	}
	public void setSop_name(String sop_name) {
		this.sop_name = sop_name;
	}
	public String getTxt_name() {
		return txt_name;
	}
	public void setTxt_name(String txt_name) {
		this.txt_name = txt_name;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	boolean ecephd,otherdeg,pgcomplete,gate;
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}
	public String getXth_board() {
		return xth_board;
	}
	public void setXth_board(String xth_board) {
		this.xth_board = xth_board;
	}
	public String getXiith_board() {
		return xiith_board;
	}
	public void setXiith_board(String xiith_board) {
		this.xiith_board = xiith_board;
	}
	public String getCollege() {
		return college;
	}
	public void setCollege(String college) {
		this.college = college;
	}
	public String getDep() {
		return dep;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAchievements() {
		return achievements;
	}
	public void setAchievements(String achievements) {
		this.achievements = achievements;
	}
	public double getXth_marks() {
		return xth_marks;
	}
	public void setXth_marks(double xth_marks) {
		this.xth_marks = xth_marks;
	}
	public double getXiith_marks() {
		return xiith_marks;
	}
	public void setXiith_marks(double xiith_marks) {
		this.xiith_marks = xiith_marks;
	}
	public double getGrad_marks() {
		return grad_marks;
	}
	public void setGrad_marks(double grad_marks) {
		this.grad_marks = grad_marks;
	}
	public double getGrad_GPA() {
		return grad_GPA;
	}
	public void setGrad_GPA(double grad_GPA) {
		this.grad_GPA = grad_GPA;
	}
	public int getXth_pass() {
		return xth_pass;
	}
	public void setXth_pass(int xth_pass) {
		this.xth_pass = xth_pass;
	}
	public int getXiith_pass() {
		return xiith_pass;
	}
	public void setXiith_pass(int xiith_pass) {
		this.xiith_pass = xiith_pass;
	}
	public int getGrad_year() {
		return grad_year;
	}
	public void setGrad_year(int grad_year) {
		this.grad_year = grad_year;
	}
	public int getTotal_marks() {
		return total_marks;
	}
	public void setTotal_marks(int total_marks) {
		this.total_marks = total_marks;
	}
	public boolean isEcephd() {
		return ecephd;
	}
	public void setEcephd(boolean ecephd) {
		this.ecephd = ecephd;
	}
	public boolean isOtherdeg() {
		return otherdeg;
	}
	public void setOtherdeg(boolean otherdeg) {
		this.otherdeg = otherdeg;
	}
	public boolean isPgcomplete() {
		return pgcomplete;
	}
	public void setPgcomplete(boolean pgcomplete) {
		this.pgcomplete = pgcomplete;
	}
	public boolean isGate() {
		return gate;
	}
	public void setGate(boolean gate) {
		this.gate = gate;
	}
}
