package Admin;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.DB;
import db.ExtraClasses;
//http://stackoverflow.com/questions/18246053/how-can-i-create-a-link-to-a-local-file-on-a-locally-run-web-page
//http://www.javatpoint.com/servlet-login-and-logout-example-using-cookies
//use post to remove all the info from url :p
//http://www.javatpoint.com/servlet-http-session-login-and-logout-example

@WebServlet("/FilterResult")
public class FilterResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Vector<ExtraClasses> A = new Vector<ExtraClasses>(DB.getRecoreds());
	
	String name, email, enroll;
	String cat, gender, pd, DOB, d;
	Date dt;
	LocalDate date = null; 
	
	String gUniv , pgUniv, phdstrm, gdeg, pgdeg, xbd, xiibd; 
	String gdep,pgdep, gstate, pgstate;
	// maintain a sesssion later :P
	private void filterPersonal() {
		Vector<ExtraClasses> temp = new Vector<ExtraClasses>();
		
		if (name != null && !name.isEmpty())
		{
			for (ExtraClasses a : A) 
				if (a.getP().getName().toLowerCase().contains(name.toLowerCase()))
					temp.add(a);
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (email != null && !email.isEmpty()) {
			for (ExtraClasses a : A)
				if (a.getP().getEmail().equals(email))
					temp.add(a);
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
	
		if (email != null && !enroll.isEmpty())
		{
			for (ExtraClasses a : A)
				if (a.getP().getEnroll().contains(enroll))
					temp.add(a);
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!cat.equals("-")) {
			for (ExtraClasses a : A)
				if (a.getP().getCategory().equals(cat))
					temp.add(a);
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (gender != null) {
			for (ExtraClasses a : A)
				if (a.getP().getGender().equals(gender))
					temp.add(a);
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (pd != null) {
			for (ExtraClasses a : A) {
				if (a.getP().getDisabled().equals(pd))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (DOB != null) {
//			DateTimeFormatter f = DateTimeFormatter.ofPattern("dd-MM-yy");
			try{
				date = LocalDate.parse(d);
				if (DOB.equals("Before")) {
					for (ExtraClasses a : A) {
//						System.out.println(a.getP().getDob());
						if (a.getP().getDob().isBefore(date))
							temp.add(a);
					}
					A.clear();
					A.addAll(temp);
					temp.clear();
				}
				if (DOB.equals("On")) {
					for (ExtraClasses a : A) {
						if (a.getP().getDob().isEqual(date))
							temp.add(a);
					}
					A.clear();
					A.addAll(temp);
					temp.clear();
				}
				if (DOB.equals("After")) {
					for (ExtraClasses a : A) {
						if (a.getP().getDob().isAfter(date))
							temp.add(a);
					}
					A.clear();
					A.addAll(temp);
					temp.clear();
				}
			}catch(Exception e)
			{
				e.printStackTrace();
				//print error msg to choose a date
			}
			
		}
	}

	void filterCheckbox(String G, String E, String L, String t, int c) {
		System.out.println("6666666666666");
		if(G == null && E == null && L == null) return;
		System.out.println(G);
		System.out.println(E);
		System.out.println(L);
		System.out.println(t);
		if (t.isEmpty() && (G != null|| E!= null|| L != null) ) {
//			lab1.setText("Please fill valid marks/score");
//			lab1.setTextFill(Color.RED);
			return;
		}
		try {
			double per = Double.parseDouble(t);
			double a_mark;
			ArrayList<ExtraClasses> temp = new ArrayList<ExtraClasses>();
			if (G != null && E != null) {
				for (ExtraClasses a : A) {
					if(c == 1)	a_mark = a.getE().getXth_marks();
					else if(c == 2) a_mark = a.getE().getXiith_marks();
					else if(c == 3) a_mark = a.getE().getGrad_marks();
					else if(c == 4){
						if(a.getE().isPgcomplete())
							a_mark = a.getPg().getMarks();
						else continue;
					}
					else{
						if(a.getE().isGate()){
							Float f = new Float(a.getPhdgate().getScore());
							a_mark = f.doubleValue();
						}else continue;
					}
					if (a_mark >= per)
						temp.add(a);
				}
				A.clear();
				A.addAll(temp);
				temp.clear();
			}
			else if(G != null && L != null)
			{
				for (ExtraClasses a : A) {
					if(c == 1)	a_mark = a.getE().getXth_marks();
					else if( c == 2) a_mark = a.getE().getXiith_marks();
					else if(c == 3) a_mark = a.getE().getGrad_marks();
					else if(c == 4){
						if(a.getE().isPgcomplete())
							a_mark = a.getPg().getMarks();
						else continue;
					}
					else{
						if(a.getE().isGate()){
							Float f = new Float(a.getPhdgate().getScore());
							a_mark = f.doubleValue();
						}else continue;
					}
					if (a_mark != per)
						temp.add(a);
				}
				A.clear();
				A.addAll(temp);
				temp.clear();
			}
			else if(L != null && E != null)
			{
				for (ExtraClasses a : A) {
					if(c == 1)	a_mark = a.getE().getXth_marks();
					else if( c == 2) a_mark = a.getE().getXiith_marks();
					else if(c == 3) a_mark = a.getE().getGrad_marks();
					else if(c == 4){
						if(a.getE().isPgcomplete())
							a_mark = a.getPg().getMarks();
						else continue;
					}
					else{
						if(a.getE().isGate()){
							Float f = new Float(a.getPhdgate().getScore());
							a_mark = f.doubleValue();
						}else continue;
					}
					if (a_mark <= per)
						temp.add(a);
				}
				A.clear();
				A.addAll(temp);
				temp.clear();
			}
			else if(L != null){
				for (ExtraClasses a : A) {
					if(c == 1)	a_mark = a.getE().getXth_marks();
					else if( c == 2) a_mark = a.getE().getXiith_marks();
					else if(c == 3) a_mark = a.getE().getGrad_marks();
					else if(c == 4){
						if(a.getE().isPgcomplete())
							a_mark = a.getPg().getMarks();
						else continue;
					}
					else{
						if(a.getE().isGate()){
							Float f = new Float(a.getPhdgate().getScore());
							a_mark = f.doubleValue();
						}else continue;
					}
					if (a_mark < per)
						temp.add(a);
				}			
				A.clear();
				A.addAll(temp);
				temp.clear();
			}
			else if(G != null)
			{
				for (ExtraClasses a : A) {
					if(c == 1)	a_mark = a.getE().getXth_marks();
					else if( c == 2) a_mark = a.getE().getXiith_marks();
					else if(c == 3) a_mark = a.getE().getGrad_marks();
					else if(c == 4){
						if(a.getE().isPgcomplete())
							a_mark = a.getPg().getMarks();
						else continue;
					}
					else{
						if(a.getE().isGate()){
							Float f = new Float(a.getPhdgate().getScore());
							a_mark = f.doubleValue();
						}else continue;
					}
					if (a_mark > per)
						temp.add(a);
				}
				A.clear();
				A.addAll(temp);
				temp.clear();
			}
			else if(E != null){
				for (ExtraClasses a : A) {
					if(c == 1)	a_mark = a.getE().getXth_marks();
					else if( c == 2) a_mark = a.getE().getXiith_marks();
					else if(c == 3) a_mark = a.getE().getGrad_marks();
					else if(c == 4){
						if(a.getE().isPgcomplete())
							a_mark = a.getPg().getMarks();
						else continue;
					}
					else{
						if(a.getE().isGate()){
							Float f = new Float(a.getPhdgate().getScore());
							a_mark = f.doubleValue();
						}else continue;
					}
					if (a_mark == per)
						temp.add(a);
				}
				A.clear();
				A.addAll(temp);
				temp.clear();
			}		
		} catch (Exception e) {
			
		}
	}
	
	private void filterEdu() {
		Vector<ExtraClasses> temp = new Vector<ExtraClasses>();
		if (!phdstrm.equals("-")) {	
			for (ExtraClasses a : A) {
				System.out.println("232142  : "+a.getP().getStream());
				if (a.getP().getStream().equals(phdstrm))
					temp.add(a);
			}	
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!gdeg.equals("-")) {
			for (ExtraClasses a : A) {
				if (a.getE().getDegree().equals(gdeg))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!pgdeg.equals("-")) {
			for (ExtraClasses a : A) {
				if (a.getPg() != null) {
					if (a.getPg().getDeg().equals(pgdeg))
						temp.add(a);
				}
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!xbd.equals("-")) {
			for (ExtraClasses a : A) {
				if (a.getE().getXth_board().equals(xbd))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!xiibd.equals("-")) {
			for (ExtraClasses a : A) {
				if (a.getE().getXiith_board().equals(xiibd))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!gdep.equals("-")) {
			for (ExtraClasses a : A) {
				if (a.getE().getDep().equals(gdep))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!pgdep.equals("-")) {
				for (ExtraClasses a : A) {
				if (a.getPg() != null) {
					if (a.getPg().getDep().equals(pgdep))
						temp.add(a);
				}
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (gUniv != null && !gUniv.isEmpty()) {
			for (ExtraClasses a : A) {
				if (a.getE().getUniversity().equals(gUniv))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (pgUniv != null && !pgUniv.isEmpty()) {
			for (ExtraClasses a : A) {
				if (a.getPg() != null) {
					if (a.getPg().getColl().equals(pgUniv))
						temp.add(a);
				}
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!gstate.equals("-")) {
			for (ExtraClasses a : A) {
				if (a.getE().getState().equals(gstate))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
		if (!pgstate.equals("-"))
		{			
			for (ExtraClasses a : A) {
				if (a.getPg() != null) {
					if (a.getPg().getState().equals(pgstate))
						temp.add(a);
				}
			}
			A.clear();
			A.addAll(temp);
			temp.clear();
		}
	}

  	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  		
  		System.out.println("huihuihiuhiuhi");
  		A.clear();
		A.addAll(DB.getRecoreds());
		System.out.println(A.size());
			
  		email = request.getParameter("email");
  		name = request.getParameter("name");
  		enroll = request.getParameter("enroll");
  		cat = request.getParameter("categ");
  		gender = request.getParameter("gender");
  		pd = request.getParameter("pwd");
  		DOB = request.getParameter("dob");
  		d = request.getParameter("DOB");
  		
  		System.out.println(email + name + enroll + cat+ gender + pd+ DOB + d);
  		filterPersonal();
  		
  		phdstrm = request.getParameter("phdStream");
  		gdeg = request.getParameter("gradDeg");
  		pgdeg = request.getParameter("postGradDeg");
  		xbd = request.getParameter("Xboard");
  		xiibd = request.getParameter("XIIboard");
  		gdep = request.getParameter("gradDep");
  		pgdep = request.getParameter("postGradDep");
  		gUniv = request.getParameter("univGrad");
  		pgUniv = request.getParameter("univPostGrad");
  		gstate = request.getParameter("stateGrad");
  		pgstate = request.getParameter("statePostGrad");

  		System.out.println(phdstrm + gdeg + pgdeg + xbd + xiibd + gdep + pgdep + gUniv+ pgUniv + gstate + pgstate);
  		filterEdu();

  		filterCheckbox(request.getParameter("Xper1"), request.getParameter("Xper2"), request.getParameter("Xper3"), request.getParameter("Xper"),1);
  		filterCheckbox(request.getParameter("XIIper1"), request.getParameter("XIIper2"), request.getParameter("XIIper3"), request.getParameter("XIIper"),2);
  		filterCheckbox(request.getParameter("Gradper1"), request.getParameter("Gradper2"), request.getParameter("Gradper3"), request.getParameter("Gradper"),3);
  		filterCheckbox(request.getParameter("postGradper1"), request.getParameter("postGradper2"), request.getParameter("postGradper3"), request.getParameter("postGradper"),4);
  		filterCheckbox(request.getParameter("GateScore1"), request.getParameter("GateScore2"), request.getParameter("GateScore3"), request.getParameter("GateScore"),5);
  		
  		String uptodt ,fromdt;
  		uptodt = request.getParameter("upto");
  		fromdt = request.getParameter("from");
  		Vector<ExtraClasses> temp = new Vector<ExtraClasses>();
  		
  		if(!uptodt.isEmpty())
  		{
  			LocalDate upto = LocalDate.parse(uptodt);
  			Date uptod = Date.from(upto.atStartOfDay(ZoneId.systemDefault()).toInstant());
			for(ExtraClasses a:A)
			{	
				if(a.getTime_of_applying().before(uptod))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();

  		}
  		if(!fromdt.isEmpty()){
  			LocalDate from = LocalDate.parse(fromdt);
			Date fromd = Date.from(from.atStartOfDay(ZoneId.systemDefault()).toInstant());
			for(ExtraClasses a:A)
			{
				if(a.getTime_of_applying().after(fromd))
					temp.add(a);
			}
			A.clear();
			A.addAll(temp);
			temp.clear();

  		}
//		System.out.println(uptodt  + fromdt);
//		System.out.println(A.get(0).getP().getName() + "000gdgdsa :" + A.size());
//		System.out.println(A.get(0).getE().getCv_name());
		request.setAttribute("filteredData", A);
		request.getRequestDispatcher("admin/filterPage.jsp").forward(request, response);;
		
  	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}
}