package Admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.nio.file.Path;
import java.nio.file.Paths;

import db.DB;
import db.ExtraClasses;

public class ReadRecords {
	private static ObjectInputStream in = null;
	static Path p = Paths.get("C:\\Users\\Aishwarya\\Documents\\gitjava\\ap_prj_ii\\AP_PRJ_II\\ApplicantData");
	public static synchronized void readRecords() throws IOException, ClassNotFoundException
	{
		try 
		{
			File folder = new File(p.toString());
			File[] a = folder.listFiles();
			System.out.println("ddddddddddd " + a.length);
			for(File b:a)
			{
				if(b.isDirectory())
				{
					File c[]= b.listFiles();
					System.out.println("ffffffffffff "+c.length);
					for(File d:c)
					{
						String p=d.getAbsolutePath();
						if(p.endsWith(".dat"))
						{
							in=new ObjectInputStream(new FileInputStream(p));
							try
							{
								ExtraClasses temp=(ExtraClasses)in.readObject();
								DB.updateRecords(temp);
							}
							catch(IOException e)
							{
								e.printStackTrace();
								System.out.println("...........");
//								return;
							}
							finally
							{
								if(in != null) in.close();
							}
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("No Database!");
			return;
		}
		finally 
		{ 
			if (in != null)
				in.close();
		}
	}
}
