package Admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.DB;
import db.ExtraClasses;
/**
 * Servlet implementation class Filter
 */
@WebServlet("/Filter")
public class Filter1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpSession session;
    Vector<ExtraClasses> records = DB.getRecoreds();
   
    String[] states = new String[] {"Andaman and Nicobar Islands","Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh",
			"Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh",
			"Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra",
			"Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim",
			"Tamil Nadu", "Tripura", "Uttaranchal", "Uttar Pradesh", "West Bengal"
	};
    
    ArrayList<String> graddeg = new ArrayList<String>(); 
	ArrayList<String> pgdeg = new ArrayList<String>();
	ArrayList<String> xboard = new ArrayList<String>();
	ArrayList<String> xiiboard = new ArrayList<String>();
	ArrayList<String> graddep = new ArrayList<String>();
	ArrayList<String> pgdep = new ArrayList<String>();

    void initializeForm()
    {
    	try{
    		if(records.size() == 0) {
    			System.out.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
    			ReadRecords.readRecords();
    		}
        			
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	System.out.print( "fdfdf "+ records.size());
    	for (ExtraClasses a : records) {
			String d = a.getE().getDegree();
			String xb = a.getE().getXth_board();
			String xiib = a.getE().getXiith_board();
			String gdep = a.getE().getDep();

			if (!graddep.contains(gdep) && gdep.compareTo("-") != 0)
				graddep.add(gdep);
			if (!xiiboard.contains(xiib) && xiib.compareTo("-") != 0)
				xiiboard.add(xiib);
			if (!graddeg.contains(d) && d.compareTo("-") != 0)
				graddeg.add(d);
			if (!xboard.contains(xb) && xb.compareTo("-") != 0)
				xboard.add(xb);
			if (a.getPg() != null) {
				String pgd = a.getPg().getDeg();
				String pdep = a.getPg().getDep();
				if (!pgdeg.contains(pgd) && pgd.compareTo("-") != 0)
					pgdeg.add(pgd);
				if (!pgdep.contains(pdep) && pdep.compareTo("-") != 0)
					pgdep.add(pdep);
			}
		}
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		request.setAttribute("states", states);
		
		initializeForm();
		request.setAttribute("graddeg", graddeg);
		request.setAttribute("pgdeg", pgdeg);
		request.setAttribute("xboard", xboard);
		request.setAttribute("xiiboard", xiiboard);
		request.setAttribute("graddep", graddep);
		request.setAttribute("pgdep", pgdep);
		System.out.println("hellooooooooooo" + graddeg.get(0));
		
		System.out.println("yeyayay" + states[1]);
		request.getRequestDispatcher("admin/FirstPage.jsp").forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		w.println("hey");
	}
//	<%@ taglib prefix="c" 
//	           uri="http://java.sun.com/jsp/jstl/core" %>
//	   <c:forEach items="${states}" var="line">
//			 <option value= '${line}'><c:out value="${line}" /></option>
//	</c:forEach>

}
