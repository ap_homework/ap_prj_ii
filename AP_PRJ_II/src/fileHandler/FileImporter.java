package fileHandler;

//@author1:Harshvardhan Kalra 2014043
//@author2:Aishwarya Jaiswal 2014007

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import db.Educational_info;
import db.ExtraClasses;
import db.OtherDeg;
import db.Personal_info;
import db.Pg;
import db.PhDECE;
import db.PhDGATE;

public class FileImporter {
	
	BufferedReader br = null;
	ExtraClasses E;
	Pattern ptn = Pattern.compile("\"([^\"]*)\"");
	ArrayList<String> adr;
	String[] word;
	DateTimeFormatter f = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	Date dt;
	public FileImporter()
	{
		try{
			importF();
		}catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("file doesnt exist");
		}
	}

	void setE()
	{
		E = new ExtraClasses();
		Personal_info p = new Personal_info();
		p.setEmail(word[0]);
		p.setName(word[1]);
		p.setCorraddr(adr.get(0));
		p.setMob(word[3]);
		p.setStream(word[4]);
		String[] pref = new String[3];
		pref[0] = new String(word[5]);
		pref[1] = new String(word[6]);
		pref[2] = new String(word[7]);
		p.setPreference(pref);
		p.setGender(word[8]);
		p.setCategory(word[9]);
		p.setDisabled(word[10]);
		LocalDate d = LocalDate.parse(word[11], f);
		p.setDob(d);
		p.setDefense(word[12]);
		p.setFname(word[13]);
		p.setNationality(word[14]);
		p.setPermaddr(adr.get(1));
		p.setPin(word[16]);
		E.setP(p);
		
		Educational_info e = new Educational_info();
		e.setXth_board(word[17]);
		e.setXth_marks(Double.parseDouble(word[18]));
		e.setXth_pass(Integer.parseInt(word[19]));
		e.setXiith_board(word[20]);
		e.setXiith_marks(Double.parseDouble(word[21]));
		e.setXiith_pass(Integer.parseInt(word[22]));
		e.setDegree(word[23]);
		e.setDep(word[24]);
		e.setCollege(word[25]);
		e.setUniversity(word[26]);
		e.setCity(word[27]);
		e.setState(word[28]);
		e.setGrad_year(Integer.parseInt(word[29]));
		if( word[30].split(":")[0].equals("CGPA")){
			e.setGrad_GPA(Double.parseDouble(word[30].split(":")[1]));
			e.setGrad_marks(0.0);
			e.setTotal(10.0);
		}
			
		else{
			e.setGrad_marks(Double.parseDouble(word[30].split(":")[1]));
			e.setGrad_GPA(0.0);
		}
			
		if(word[31].equals("No") || word[31] == null || word[31].equals(""))
			e.setEcephd(false);
		else 
		{
			e.setEcephd(true);
			PhDECE ece = new PhDECE();
			String[] prf = new String[4];
			prf[0] = new String(word[32]);
			prf[1] = new String(word[33]);
			prf[2] = new String(word[34]);
			prf[3] = new String(word[35]);
			ece.setEce_preferences(prf);
			E.setPhdece(ece);
		}
		if(word[36].equals("No") || word[36] == null || word[36].equals(""))
			e.setPgcomplete(false);
		else
		{
			e.setPgcomplete(true);
			Pg pg = new Pg();
			pg.setDeg(word[37]);
			pg.setDep(word[38]);
			pg.setColl(word[39]);
			pg.setTitle(word[40]);
			pg.setCity(word[41]);
			pg.setState(word[42]);
			pg.setYr(Integer.parseInt(word[43]));
			if( word[44].split(":")[0].equals("CGPA")){
				pg.setCgpa(Double.parseDouble(word[44].split(":")[1]));
				pg.setMarks(0.0);
				pg.setTotal(10.0);
			}
			else
			{
				pg.setMarks(Double.parseDouble(word[44].split(":")[1]));
				pg.setCgpa(0.0);
			}
			E.setPg(pg);
		}
		if(word[45].equals("No") || word[45] == null || word[45].equals(""))
			e.setOtherdeg(false);
		else{
			OtherDeg o = new OtherDeg();
			e.setOtherdeg(true);
			o.setExam(word[46]);
			o.setSubj(word[47]);
			o.setYr(Integer.parseInt(word[48]));
			o.setScore(Integer.parseInt(word[49]));
			o.setRank(Integer.parseInt(word[50]));
			E.setO(o);
		}
		if(word[51].equals("No") || word[51] == null || word[51].equals(""))
			e.setGate(false);
		else{
			e.setGate(true);
			PhDGATE pgate = new PhDGATE();
			pgate.setArea(word[52]);
			pgate.setYear(Integer.parseInt(word[53]));
			pgate.setMarks(Float.parseFloat(word[54]));
			pgate.setScore(Float.parseFloat(word[55]));
			pgate.setRank(Integer.parseInt(word[56]));
			E.setPhdgate(pgate);
		}
		e.setAchievements(word[57]);
		E.setE(e);
		DateFormat ft = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.ENGLISH);
		try{
			dt = ft.parse(word[58]);
			E.setTime_of_applying(dt);
//			String enrollID = word[59];

		}catch(Exception s)
		{
			s.printStackTrace();
		}
	}
	public void importF() throws IOException {
		
//		Path p = Paths.get("src", "Data_to_Import.csv");
		Path p = Paths.get("C:\\Users\\Aishwarya\\Documents\\gitjava\\ap_prj_ii\\AP_PRJ_II\\src\\Data_to_Import.csv");
		br = new BufferedReader(new FileReader(p.toString()));
		String line = "";
		adr = new ArrayList<String>();
		br.readLine();
		while((line = br.readLine()) != null) {
			Matcher m = ptn.matcher(line);
			if (adr.size() > 0)
				adr.clear();
			while(m.find()){
				adr.add(new String(m.group(1).trim()));				
			}
			line = line.replaceAll("\"" +adr.get(0) + "\"", "");
			line = line.replaceAll("\"" +adr.get(1) + "\"", "");
			word = line.split(",");
			setE();
//			E.getP().setEnroll("PhD" + DB.getCurrEnrollNumber());
//			DB.updateRecords(E);
			FileWrite.write_to_file(E);
		}
	}
	public static void main(String[] arg)
	{
		new FileImporter();
	}
}