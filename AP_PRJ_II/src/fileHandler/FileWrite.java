package fileHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;

import db.DB;
import db.Educational_info;
import db.ExtraClasses;
import db.OtherDeg;
import db.Personal_info;
import db.Pg;
import db.PhDECE;
import db.PhDGATE;

public class FileWrite implements Serializable {
	private static final long serialVersionUID = 1L;
	static Path tPath = Paths.get("C:\\Users\\Aishwarya\\Documents\\gitjava\\ap_prj_ii\\AP_PRJ_II\\t.txt");
	public static synchronized void write_to_file(ExtraClasses E) throws IOException {
		user_record_gen(E);
	}

	static void user_record_gen(ExtraClasses E) throws IOException
	{
		//need to change the path of the folders for it to run :||
		BufferedReader reader = null;
		BufferedWriter writer = null;
		try
		{
			try{
				if(DB.getCurrEnrollNumber() == -1){
					reader=new BufferedReader(new FileReader(tPath.toString()));
					int curr_enrollnum=Integer.parseInt(reader.readLine());
					DB.setCurrEnrollNumber(curr_enrollnum);
					try
					{
						reader.close();
					}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}				
			}catch(IOException e){
				DB.setCurrEnrollNumber(1001);		
			}finally
			{
				writer = new BufferedWriter(new FileWriter(tPath.toString()));
				E.getP().setEnroll("PhD"+DB.getCurrEnrollNumber()+"");
				DB.incCurrEnrollNumber();
				writer.write(""+DB.getCurrEnrollNumber());
				writer.close();
			}
			DB.updateRecords(E);
			
			String enrl = E.getP().getEnroll();
			File folder = new File("ApplicantData");	//height of irony :p
			if (!folder.exists())
			{
				if (folder.mkdir())
					System.out.println("Applicant data stored in directory ApplicantData");
				else
					System.out.println("Failure");
			}
			File usrfolder = new File("ApplicantData" + File.separator + enrl);
			if (!usrfolder.exists())
			{
				if (usrfolder.mkdir())
					System.out.println("Stored data for applicant "+enrl);
				else 
					System.out.println("No data stored for applcant "+enrl);
			}
			
			ObjectOutputStream usrwriter=new ObjectOutputStream(new FileOutputStream("ApplicantData"+File.separator+enrl+File.separator+enrl+".dat"));
			File orig_cv=new File("tempCV.pdf");
			File stored_cv=new File("ApplicantData"+File.separator+enrl+File.separator+enrl+"_CV.pdf");
			File orig_sop=new File("tempSOP.pdf");
			File stored_sop=new File("ApplicantData"+File.separator+enrl+File.separator+enrl+"_SOP.pdf");
			File applicant_data=new File("ApplicantData"+File.separator+enrl+File.separator+"ApplicantData.txt");
			orig_cv.renameTo(stored_cv);
			orig_sop.renameTo(stored_sop);
			
			E.getE().setCv_name(stored_cv.getAbsolutePath());
			E.getE().setSop_name(stored_sop.getAbsolutePath());
			E.getE().setTxt_name(applicant_data.getAbsolutePath());
			
			usrwriter.writeObject(E);
			usrwriter.close();
			ApplicantRecordSave(E);
			
//			if(new File("src/Applicants_info.dat").delete())
//				System.out.println("Deleted local copy of applicant data");
			if(orig_cv.delete())
				System.out.println("Deleted local CV");
			if(orig_sop.delete())
				System.out.println("Deleted local SOP");
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
		
	private static void ApplicantRecordSave(ExtraClasses E)
	{
		BufferedWriter writer= null;
	    try
	    {
	    	File f = new File("ApplicantData");
	    	String s = "ApplicantData"+File.separator+E.getP().getEnroll()+File.separator+"ApplicantData.txt";
	    	writer = new BufferedWriter(new FileWriter(s));
	    	Personal_info x = E.getP();
	    	Educational_info y = E.getE();
	    	PhDECE p = E.getPhdece();
	    	Pg z = E.getPg();
	    	PhDGATE a = E.getPhdgate();
	    	OtherDeg b = E.getO();
	    	
//	    	E.getE().setTxt_name(f.getAbsolutePath()+File.separator+E.getP().getEnroll()+File.separator+"ApplicantData.txt");
	    	
	    	writer.write("Enrollment Number : "+E.getP().getEnroll()+"\n");
	    	writer.write("Timestamp : "+ E.getTime_of_applying()+"\n\n");
	    	
	    	writer.write("PERSONAL DETAILS:\n");
	    	writer.write("Email : "+x.getEmail()+"\n");
	    	writer.write("Name : "+x.getName()+"\n");
	    	writer.write("Address of Correspondence : "+x.getCorraddr()+"\n");
	    	writer.write("Mobile : "+x.getMob()+"\n");
	    	writer.write("PhD Stream : "+x.getStream()+"\n");
	    	writer.write("PhD Preference 1 : "+x.getPreference()[0]+"\n");
	    	writer.write("PhD Preference 2 : "+x.getPreference()[1]+"\n");
	    	writer.write("PhD Preference 3 : "+x.getPreference()[2]+"\n");
	    	writer.write("Gender : "+x.getGender()+"\n");
	    	writer.write("Category : "+x.getCategory()+"\n");
	    	writer.write("Physically Disabled : "+x.getDisabled()+"\n");
	    	DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	    	writer.write("Date of Birth : "+x.getDob().format(fmt).toString()+"\n");
	    	writer.write("Children/War Widows of Defence Personnel wounded/killed in war : "+x.getDefense()+"\n");
	    	writer.write("Father's Name : "+x.getFname()+"\n");
	    	writer.write("Nationality : "+x.getNationality()+"\n");
	    	writer.write("Permanent Address : "+x.getPermaddr()+"\n");
	    	writer.write("Pincode : "+x.getPin()+"\n");
	    	writer.write("--------------------------------------------------------------\n");
	    	writer.write("SCHOOLING DETAILS:\n");
	    	writer.write("Xth Board : "+y.getXth_board()+"\n");
	    	writer.write("Xth Marks : "+y.getXth_marks()+"\n");
	    	writer.write("Year of Passing Xth : "+y.getXth_pass()+"\n");
	    	writer.write("XIIth Board : "+y.getXiith_board()+"\n");
	    	writer.write("XIIth Marks : "+y.getXiith_marks()+"\n");
	    	writer.write("Year of Passing XIIth : "+y.getXiith_pass()+"\n");
	    	writer.write("--------------------------------------------------------------\n");
	    	writer.write("GRADUATION DETAILS:\n");
	    	writer.write("Degree : "+y.getDegree()+"\n");
	    	writer.write("Department/Discipline : "+y.getDep()+"\n");
	    	writer.write("College : "+y.getCollege()+"\n");
	    	writer.write("University : "+y.getUniversity()+"\n");
	    	writer.write("City : "+y.getCity()+"\n");
	    	writer.write("State : "+y.getState()+"\n");
	    	writer.write("Year of Graduation : "+y.getGrad_year()+"\n");
	    	if(y.getGrad_GPA()==0)
	    		writer.write("Marks : "+y.getGrad_marks()+"%\n");
	    	else
	    		writer.write("CGPA: "+y.getGrad_GPA()+"/"+y.getTotal()+"\n");
	    		writer.write("----------------------------------------------------------\n");
	    	if(y.isEcephd())
	    	{
	    		  writer.write("ECE PhD\n");
	    		  writer.write("Preference 1 : "+p.getEce_preferences()[0]+"\n");
	    		  writer.write("Preference 2 : "+p.getEce_preferences()[1]+"\n");
	    		  writer.write("Preference 3 : "+p.getEce_preferences()[2]+"\n");
	    		  writer.write("Preference 4 : "+p.getEce_preferences()[3]+"\n");
	    		  writer.write("--------------------------------------------------------\n");
	    	}
	    	if(y.isPgcomplete())
	    	{
	    		writer.write("POST GRADUATION\n");
	    		writer.write("College : "+z.getColl()+"\n");
	    		writer.write("City : "+z.getCity()+"\n");
	    		writer.write("State : "+z.getState()+"\n");
	    		writer.write("Department/Discipline : "+z.getDep()+"\n");
	    		writer.write("Thesis title : "+z.getTitle()+"\n");
	    		writer.write("Year of Post-Graduation : "+z.getYr()+"\n");
	    		if(z.getCgpa()==0)
	    			writer.write("Marks : "+z.getMarks()+"%\n");
		    	else if(z.getMarks()==0)
		    		writer.write("CGPA : "+z.getCgpa()+"/"+z.getTotal()+"\n");
		    	writer.write("--------------------------------------------------------------\n");
	    	  }
	    	  if(y.isOtherdeg())
	    	  {
	    		  writer.write("OTHER DEGREE\n");
	    		  writer.write("Exam Name : "+b.getExam()+"\n");
	    		  writer.write("Subject : "+b.getSubj()+"\n");
	    		  writer.write("Year : "+b.getYr()+"\n");
	    		  writer.write("Score : "+b.getScore()+"\n");
	    		  writer.write("Rank : "+b.getRank()+"\n");
	    		  writer.write("--------------------------------------------------------------\n");
	    	  }
	    	  if(y.isGate())
	    	  {
	    		  writer.write("Area : "+a.getArea()+"\n");
	    		  writer.write("Year : "+a.getYear()+"\n");
	    		  writer.write("Marks : "+a.getMarks()+"/100\n");
	    		  writer.write("Score : "+a.getScore()+"\n");
	    		  writer.write("Rank : "+a.getRank()+"\n");
	    		  writer.write("--------------------------------------------------------------\n");
	    	  }
	    	  writer.write("Achievements :"+y.getAchievements()+"\n");
	    	  writer.close();
	      }
	      catch (Exception e)
	      {
	         e.printStackTrace();
	         System.out.println("Could not make .txt file");
	      }
	}	
}
